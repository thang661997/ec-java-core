/**
 * @author ThangHT8
 * @date 20 Sept 2023
 * @version 1.0
 */

package dao;

public class SQLCRUD {

	public static final String INSERT_NORMAL_STUDENT = "INSERT INTO Student (type, fullName, doB, sex, phoneNumber, universityName, gradeLevel, englishScore, entryTestScore) VALUES (0, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String INSERT_GOOD_STUDENT = "INSERT INTO Student (type, fullName, doB, sex, phoneNumber, universityName, gradeLevel, gpa, bestRewardName) VALUES (1, ?, ?, ?, ?, ?, ?, ?, ?)";

//	public static final String SELECT_ALL = "SELECT * FROM Student";
	
	public static final String SELECT_ALL = "SELECT id, type, fullName, doB, sex, phoneNumber, universityName, gradeLevel, gpa, bestRewardName, englishScore, entryTestScore FROM Student";

}
