package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import common.Validator;
import entities.GoodStudent;
import entities.NormalStudent;
import entities.Student;
import services.Sort;
import services.StudentService;

public class App {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		while (true) {
			System.out.println("---------------------------------");
			System.out.println("Welcome:");
			System.out.println("----------------------------------------------------------------------");
			System.out.println("1. Enter student information and save to database");
			System.out.println("2. Display list of students");
			System.out.println("3. Select successful candidates to join the company");
			System.out.println("4. Displays full name and phone number information of all students");
			System.out.println("5. Exit the program");

			int choice;
			System.out.print("Enter selection: ");
			try {
				choice = sc.nextInt();
				sc.nextLine();
			} catch (NumberFormatException e) {
				System.out.println("Invalid selection. Please re-enter.");
				continue;
			}

			switch (choice) {
			case 1:
				StudentService.inputData(sc);
				break;
			case 2:
				displayStudentList();
				break;
			case 3:
				selectCandidates(StudentService.getAll());
				break;
			case 4:
				displayAllStudents(StudentService.getAll());
				break;
			case 5:
				System.out.println("Exit the program.");
				sc.close();
				System.exit(0);
			default:
				System.out.println("Invalid selection. Please re-enter.");
			}
		}
	}

	private static void displayStudentList() {
		List<Student> students = StudentService.getAll();

		if (students.isEmpty()) {
			System.out.println("Student list is empty.");
		} else {
			System.out.println("Student list:");

			List<Student> goodStudents = new ArrayList<>();
			List<Student> normalStudents = new ArrayList<>();

			for (Student student : students) {
				if (student.getType() == 1) {
					goodStudents.add(student);
				} else if (student.getType() == 0) {
					normalStudents.add(student);
				}
			}

			if (!goodStudents.isEmpty()) {
				System.out.println("------------------");
				System.out.println("Good Student: ");
				for (Student student : goodStudents) {
					student.showMyInfor();
				}
			}

			if (!normalStudents.isEmpty()) {
				System.out.println("------------------");
				System.out.println("Normal Student: ");
				for (Student student : normalStudents) {
					student.showMyInfor();
				}
			}
		}
	}

	public static void selectCandidates(List<Student> students) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of students to recruit (minimum 11, maximum 15): ");
		int numCandidates = Validator.getIntWithRange(11, 15, sc);

		List<GoodStudent> goodStudents = new ArrayList<>();
		List<NormalStudent> normalStudents = new ArrayList<>();

		for (Student student : students) {
			if (student instanceof GoodStudent) {
				goodStudents.add((GoodStudent) student);
			} else if (student instanceof NormalStudent) {
				normalStudents.add((NormalStudent) student);
			}
		}

		System.out.println("List of candidates admitted to the company:");

		Sort.sortGoodStudent(goodStudents);

		int selectedGoodStudents = 0;

		for (GoodStudent goodStudent : goodStudents) {
			if (selectedGoodStudents >= numCandidates) {
				break;
			}
			System.out.println(goodStudent.getFullName() + " - GPA: " + goodStudent.getGpa());
			goodStudent.showMyInfor();
			selectedGoodStudents++;
		}

		int remainingCandidates = numCandidates - selectedGoodStudents;

		if (remainingCandidates > 0) {
			Sort.sortNormalStudent(normalStudents);

			System.out.println("Average candidate list:");

			int selectedNormalStudents = 0;

			for (NormalStudent normalStudent : normalStudents) {
				if (selectedNormalStudents >= remainingCandidates) {
					break;
				}
				System.out.println(normalStudent.getFullName() + " - Entry Test Score: "
						+ normalStudent.getEntryTestScore() + " - Score TOEIC: " + normalStudent.getEnglishScore());
				selectedNormalStudents++;
			}
		}
	}

	public static void displayAllStudents(List<Student> students) {
		Sort.sortAllStudent(students);

		System.out.println("Information for all students: ");

		for (Student student : students) {
			System.out.println("Full name: " + student.getFullName() + " - Phone: " + student.getPhoneNumber());
		}
	}

}
