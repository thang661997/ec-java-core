/**
 * @author ThangHT8
 * @date Apr 6, 2023
 * @version 1.0
 */

package services;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import entities.GoodStudent;
import entities.NormalStudent;
import entities.Student;

public class Sort {

	public static void sortAllStudent(List<Student> students) {
		Collections.sort(students, new Comparator<Student>() {

			@Override
			public int compare(Student o1, Student o2) {
				if (o1.getFullName().equals(o2.getFullName())) {
					return o1.getPhoneNumber().compareTo(o2.getPhoneNumber());
				} else {
					return o2.getFullName().compareTo(o1.getFullName());
				}
			}

		});
	}

	public static void sortGoodStudent(List<GoodStudent> goodStudents) {
		Collections.sort(goodStudents, new Comparator<GoodStudent>() {

			@Override
			public int compare(GoodStudent o1, GoodStudent o2) {
				if (o1.getGpa() == null || o2.getGpa() == null) {
					if (o1.getGpa() == null) {
						return 1;
					} else {
						return -1;
					}
				} else if (o1.getGpa().equals(o2.getGpa())) {
					if (o1.getFullName() == null || o2.getFullName() == null) {
						if (o1.getFullName() == null) {
							return 1;
						} else {
							return -1;
						}
					} else {
						return o1.getFullName().compareTo(o2.getFullName());
					}
				} else {
					return o2.getGpa().compareTo(o1.getGpa());
				}
			}

		});
	}

	public static void sortNormalStudent(List<NormalStudent> normalStudents) {
		Collections.sort(normalStudents, new Comparator<NormalStudent>() {

			@Override
			public int compare(NormalStudent o1, NormalStudent o2) {
				if (o1.getEnglishScore().equals(o2.getEnglishScore())) {
					return o1.getFullName().compareTo(o2.getFullName());
				} else {
					return o2.getEnglishScore().compareTo(o1.getEnglishScore());
				}
			}

		});
	}

}
