/**
 * @author ThangHT8
 * @date 20 Sept 2023
 * @version 1.0
 */

package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import common.InvalidDOBException;
import common.InvalidFullNameException;
import common.InvalidPhoneNumberException;
import common.Validator;
import dao.ConnectDB;
import dao.SQLCRUD;
import entities.GoodStudent;
import entities.NormalStudent;
import entities.Student;

public class StudentService {

	public static void inputData(Scanner sc) {
		try {
			System.out.println("Enter student information:");

			System.out.print("Full name: ");
			String fullName = sc.nextLine();

			if (!Validator.isValidName(fullName)) {
				throw new InvalidFullNameException("Full name is not valid.");
			}

			System.out.print("Date of birth (dd/MM/yyyy): ");
			String dobString = sc.nextLine();

			if (!Validator.isValidDate(dobString)) {
				throw new InvalidDOBException("Invalid date of birth.");
			}
			Date dob = new SimpleDateFormat("dd/MM/yyyy").parse(dobString);

			System.out.print("Gender (0: Male, 1: Female, 2: Other): ");
			byte sex = (byte) Validator.getIntWithRange(0, 2, sc);

			System.out.print("Phone number: ");
			String phoneNumber = sc.nextLine();

			if (!Validator.isValidPhone(phoneNumber)) {
				throw new InvalidPhoneNumberException("Invalid phone number.");
			}

			System.out.print("University name: ");
			String universityName = sc.nextLine();

			System.out.print("Grade Level: ");
			String gradeLevel = sc.nextLine();

			System.out.print("Student type (0: Normal, 1: Good): ");
			int type = sc.nextInt();
			sc.nextLine();

			if (type == 0) {
				System.out.print("English score: ");
				int englishScore = sc.nextInt();
				sc.nextLine();

				System.out.print("Entry Test Score: ");
				double entryTestScore = sc.nextDouble();
				sc.nextLine();

				insertNormalStudent(fullName, dob, sex, phoneNumber, universityName, gradeLevel, englishScore,
						entryTestScore);
			} else if (type == 1) {
				System.out.print("Grade point average (GPA): ");
				double gpa = sc.nextDouble();
				sc.nextLine();

				System.out.print("Best Reward Name: ");
				String bestRewardName = sc.nextLine();

				insertGoodStudent(fullName, dob, sex, phoneNumber, universityName, gradeLevel, gpa, bestRewardName);
			} else {
				System.out.println("Invalid student type.");
			}
		} catch (InvalidFullNameException | InvalidDOBException | InvalidPhoneNumberException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void insertNormalStudent(String fullName, Date dob, byte sex, String phoneNumber,
			String universityName, String gradeLevel, int englishScore, double entryTestScore) {
		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQLCRUD.INSERT_NORMAL_STUDENT)) {

			preparedStatement.setString(1, fullName);
			preparedStatement.setDate(2, new java.sql.Date(dob.getTime()));
			preparedStatement.setByte(3, sex);
			preparedStatement.setString(4, phoneNumber);
			preparedStatement.setString(5, universityName);
			preparedStatement.setString(6, gradeLevel);
			preparedStatement.setInt(7, englishScore);
			preparedStatement.setDouble(8, entryTestScore);

			int rowsInserted = preparedStatement.executeUpdate();

			if (rowsInserted > 0) {
				System.out.println("More student success!");
			} else {
				System.out.println("More students failed.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void insertGoodStudent(String fullName, Date dob, byte sex, String phoneNumber,
			String universityName, String gradeLevel, double gpa, String bestRewardName) {
		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQLCRUD.INSERT_GOOD_STUDENT)) {

			preparedStatement.setString(1, fullName);
			preparedStatement.setDate(2, new java.sql.Date(dob.getTime()));
			preparedStatement.setByte(3, sex);
			preparedStatement.setString(4, phoneNumber);
			preparedStatement.setString(5, universityName);
			preparedStatement.setString(6, gradeLevel);
			preparedStatement.setDouble(7, gpa);
			preparedStatement.setString(8, bestRewardName);

			int rowsInserted = preparedStatement.executeUpdate();

			if (rowsInserted > 0) {
				System.out.println("More student success!");
			} else {
				System.out.println("More students failed.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<Student> getAll() {
		List<Student> students = new ArrayList<Student>();

		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQLCRUD.SELECT_ALL)) {

			ResultSet resultSet = preparedStatement.executeQuery();

			students = getStudentList(resultSet);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return students;
	}

	private static List<Student> getStudentList(ResultSet rs) {
		List<Student> studentList = new ArrayList<>();
		try {
			while (rs.next()) {
				int type = rs.getInt("type");
				Student student;
				if (type == 1) {
					GoodStudent goodStudent = new GoodStudent();
					goodStudent.setGpa(rs.getDouble("gpa"));
					goodStudent.setBestRewardName(rs.getString("bestRewardName"));
					student = goodStudent;
				} else {
					NormalStudent normalStudent = new NormalStudent();
					normalStudent.setEnglishScore(rs.getInt("englishScore"));
					normalStudent.setEntryTestScore(rs.getDouble("entryTestScore"));
					student = normalStudent;
				}

				student.setId(rs.getInt("id"));
				student.setType((byte) type);
				student.setFullName(rs.getString("fullName"));
				student.setDoB(rs.getDate("doB"));
				student.setSex(rs.getByte("sex"));
				student.setPhoneNumber(rs.getString("phoneNumber"));
				student.setUniversityName(rs.getString("universityName"));
				student.setGradeLevel(rs.getString("gradeLevel"));

				studentList.add(student);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return studentList;
	}
}
