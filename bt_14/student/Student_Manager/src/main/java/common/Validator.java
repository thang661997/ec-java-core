/**
 * @author ThangHT8
 * @date 20 Sept 2023
 * @version 1.0
 */

package common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Validator {

	public static final Pattern NAME_PATTERN = Pattern
			.compile("^[a-zA-Z0-9](?:[a-zA-Z0-9]|[',. -](?![',. -])){8,48}[a-zA-Z0-9]?$");
	public static final Pattern PHONE_PATTERN = Pattern.compile("^(090|098|091|094|031|035|038)[\\d]{7}$");

	public static boolean isValidName(String name) {
		return NAME_PATTERN.matcher(name).matches();
	}

	public static boolean isValidPhone(String phone) {
		return PHONE_PATTERN.matcher(phone).matches();
	}

	public static boolean isValidDate(String date) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		df.setLenient(false);
		try {
			df.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static int getIntWithRange(int min, int max, Scanner sc) {
		while (true) {
			try {
				int result = Integer.parseInt(sc.nextLine());
				if (result >= min && result <= max) {
					return result;
				}
				System.out.println("Invalid number, please re enter!!!");
			} catch (Exception e) {
				System.out.println("Invalid number, please re enter!!!");
			}
		}
	}
}
