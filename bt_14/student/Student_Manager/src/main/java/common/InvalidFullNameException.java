/**
 * @author ThangHT8
 * @date Apr 6, 2023
 * @version 1.0
 */

package common;

public class InvalidFullNameException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidFullNameException(String message) {
		super(message);
	}

}
