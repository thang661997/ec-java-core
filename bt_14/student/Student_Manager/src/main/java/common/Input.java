/**
 * @author ThangHT8
 * @date Apr 6, 2023
 * @version 1.0
 */

package common;

import java.util.Scanner;

public class Input {

	@SuppressWarnings("resource")
	public static int inputNumber(String message) {
		System.out.print(message);
		return Integer.parseInt(new Scanner(System.in).nextLine());
	}

	@SuppressWarnings("resource")
	public static String inputString(String message) {
		System.out.print(message);
		return new Scanner(System.in).nextLine();
	}

}
