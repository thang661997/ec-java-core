/**
 * @author ThangHT8
 * @date 20 Sept 2023
 * @version 1.0
 */

package entities;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class Student {

	protected Integer id;
	protected Byte type;
	protected String fullName;
	protected Date doB;
	protected Byte sex;
	protected String phoneNumber;
	protected String universityName;
	protected String gradeLevel;

	public abstract void showMyInfor();

}
