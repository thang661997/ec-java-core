/**
 * @author ThangHT8
 * @date 20 Sept 2023
 * @version 1.0
 */

package entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NormalStudent extends Student {

	private Integer englishScore;
	private Double entryTestScore;

	@Override
	public void showMyInfor() {
		System.out.println("Normal Student [" + "id = " + super.id + ", type = " + super.type + ", fullName = "
				+ super.fullName + ", doB = " + super.doB + ", sex = " + super.sex + ", phoneNumber = "
				+ super.phoneNumber + ", universityName = " + super.universityName + ", gradeLevel = "
				+ super.gradeLevel + ", englishScore = " + englishScore + ", entryTestScore = " + entryTestScore + "]");
	}

}
