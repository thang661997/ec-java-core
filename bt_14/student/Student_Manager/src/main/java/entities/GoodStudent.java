/**
 * @author ThangHT8
 * @date 20 Sept 2023
 * @version 1.0
 */

package entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GoodStudent extends Student {

	private Double gpa;
	private String bestRewardName;

	@Override
	public void showMyInfor() {
		System.out.println("Good Student [" + "id = " + super.id + ", type = " + super.type + ", fullName = "
				+ super.fullName + ", doB = " + super.doB + ", sex = " + super.sex + ", phoneNumber = "
				+ super.phoneNumber + ", universityName = " + super.universityName + ", gradeLevel = "
				+ super.gradeLevel + ", gpa = " + gpa + ", bestRewardName = " + bestRewardName + "]");
	}

}
