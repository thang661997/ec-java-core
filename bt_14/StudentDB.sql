create database StudentDB;

create table Student (
	id  int  primary key  IDENTITY(1,1),
	type bit check (type in (0,1)),
	fullName	varchar(255) not null,
	doB		date,
	sex		tinyint check (sex in (0, 1, 2)) ,
	phoneNumber  varchar(10) unique,
	universityName	varchar(255),
	gradeLevel	varchar(255),
	gpa		float check(gpa >= 0 and gpa <= 10),
	bestRewardName	varchar(255),
	englishScore	int check(englishScore >=  0 and englishScore <= 1000),
	entryTestScore  float check(entryTestScore >= 0 and entryTestScore <= 10)
)

