/**
 * @author ThangHT8
 * @date Apr 6, 2023
 * @version 1.0
 */

package common;

public class InvalidDOBException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidDOBException(String message) {
		super(message);
	}
}
