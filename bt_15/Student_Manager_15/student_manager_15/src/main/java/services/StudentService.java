/**
 * @author ThangHT8
 * @date 25 Sept 2023
 * @version 1.0
 */

package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import common.InvalidDOBException;
import common.Validator;
import entities.PartTimeStudent;
import entities.RegularStudent;
import entities.Student;

public class StudentService {

	public static void inputData(Scanner sc, Connection connection) {
		Student student = inputStudent(sc);
		if (student != null) {
			insertStudent(student, connection);
		}
	}

	public static Student inputStudent(Scanner sc) {
		try {
			System.out.println("Enter student information: ");

			System.out.print("Full name: ");
			String fullName = sc.nextLine();

			System.out.print("Data of birth (dd/MM/yyyy): ");
			String dobString = sc.nextLine();

			if (!Validator.isValidDate(dobString)) {
				throw new InvalidDOBException("Invalid date of birth.");
			}

			Date dob = new SimpleDateFormat("dd/MM/yyyy").parse(dobString);

			System.out.print("Enrollment Year: ");
			int enrollmentYear = sc.nextInt();
			sc.nextLine();

			System.out.print("Entrance Exam Score: ");
			float entranceExamScore = sc.nextFloat();
			sc.nextLine();

			System.out.print("Student Type (0: Part-Time, 1: Regular): ");
			int studentType = sc.nextInt();
			sc.nextLine();

			if (studentType == 0) {
				System.out.print("Linked Location: ");
				String linkedLocation = sc.nextLine();

				return new PartTimeStudent(null, fullName, dob, enrollmentYear, entranceExamScore, linkedLocation);
			} else if (studentType == 1) {
				return new RegularStudent(null, fullName, dob, enrollmentYear, entranceExamScore);
			} else {
				System.out.println("Invalid student type.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void insertStudent(Student student, Connection connection) {
		try {
			String insertQuery = "INSERT INTO Student (type, fullName, doB, enrollmentYear, entranceExamScore, linkedLocation, courseName, semesterName, gpa) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

			PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
			if (student instanceof PartTimeStudent) {
				preparedStatement.setInt(1, 0); // Part-Time student
				preparedStatement.setString(6, ((PartTimeStudent) student).getLinkedLocation());
				preparedStatement.setString(7, null); // Set courseName and semesterName to null for Part-Time student
				preparedStatement.setFloat(8, 0.0f); // Set GPA to 0.0 for Part-Time student
			} else if (student instanceof RegularStudent) {
				preparedStatement.setInt(1, 1); // Regular student
				preparedStatement.setString(6, null); // Set linkedLocation to null for Regular student
				preparedStatement.setString(7, null); // Set courseName and semesterName to null for Regular student
				preparedStatement.setFloat(8, 0.0f); // Set GPA to 0.0 for Regular student
			} else {
				System.out.println("Invalid student type.");
				return;
			}

			preparedStatement.setString(2, student.getFullName());
			preparedStatement.setDate(3, new java.sql.Date(student.getDoB().getTime()));
			preparedStatement.setInt(4, student.getEnrollmentYear());
			preparedStatement.setFloat(5, student.getEntranceExamScore());

			int rowsInserted = preparedStatement.executeUpdate();

			if (rowsInserted > 0) {
				System.out.println("Student added successfully!");
			} else {
				System.out.println("Student insertion failed.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

//	public static List<Student> getAllStudents(Connection connection) {
//		List<Student> students = new ArrayList<>();
//
//		try {
//			String selectQuery = "SELECT * FROM Student";
//
//			PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
//
//			ResultSet resultSet = preparedStatement.executeQuery();
//
//			while (resultSet.next()) {
//				int type = resultSet.getInt("type");
//				Student student;
//				if (type == 0) {
//					PartTimeStudent partTimeStudent = new PartTimeStudent(resultSet.getInt("id"),
//							resultSet.getString("fullName"), resultSet.getDate("doB"),
//							resultSet.getInt("enrollmentYear"), resultSet.getFloat("entranceExamScore"),
//							resultSet.getString("linkedLocation"));
//					student = partTimeStudent;
//				} else if (type == 1) {
//					RegularStudent regularStudent = new RegularStudent(resultSet.getInt("id"),
//							resultSet.getString("fullName"), resultSet.getDate("doB"),
//							resultSet.getInt("enrollmentYear"), resultSet.getFloat("entranceExamScore"));
//					student = regularStudent;
//				} else {
//					System.out.println("Invalid student type.");
//					continue;
//				}
//
//				student.setType((byte) type);
//				student.setCourseName(resultSet.getString("courseName"));
//				student.setSemesterName(resultSet.getString("semesterName"));
//				student.setGpa(resultSet.getFloat("gpa"));
//
//				students.add(student);
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//		return students;
//	}
}
