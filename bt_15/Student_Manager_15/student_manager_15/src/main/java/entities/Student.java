/**
 * @author ThangHT8
 * @date 24 Sept 2023
 * @version 1.0
 */

package entities;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public abstract class Student implements Comparable<Student> {

	protected Integer id;
	protected Byte type;
	protected String fullName;
	protected Date doB;
	protected Integer enrollmentYear;
	protected Float entranceExamScore;
	protected String courseName;
	protected String semesterName;
	protected float gpa;

	public Student(Byte type, String fullName, Date doB, Integer enrollmentYear, Float entranceExamScore) {
		super();
		this.fullName = fullName;
		this.doB = doB;
		this.enrollmentYear = enrollmentYear;
		this.entranceExamScore = entranceExamScore;
	}

	public Student(Integer id, Byte type, String fullName, Date doB, Integer enrollmentYear, Float entranceExamScore,
			String courseName, String semesterName, float gpa) {
		super();
		this.id = id;
		this.type = type;
		this.fullName = fullName;
		this.doB = doB;
		this.enrollmentYear = enrollmentYear;
		this.entranceExamScore = entranceExamScore;
		this.courseName = courseName;
		this.semesterName = semesterName;
		this.gpa = gpa;
	}

	public abstract boolean isRegularStudent();

	public abstract void displayStudentInfo();

	public abstract float calculateGpaBySemester(String semesterName);

	@Override
	public int compareTo(Student otherStudent) {
		int typeComparison = this.getType().compareTo(otherStudent.getType());
		if (typeComparison != 0) {
			return typeComparison;
		}

		return otherStudent.getEnrollmentYear() - this.getEnrollmentYear();
	}

}
