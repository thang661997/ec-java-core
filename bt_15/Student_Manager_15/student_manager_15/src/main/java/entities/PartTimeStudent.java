/**
 * @author ThangHT8
 * @date 24 Sept 2023
 * @version 1.0
 */

package entities;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PartTimeStudent extends Student {

	private String linkedLocation;

	public PartTimeStudent(Byte type, String fullName, Date doB, Integer enrollmentYear, Float entranceExamScore,
			String linkedLocation) {
		super(type, fullName, doB, enrollmentYear, entranceExamScore);
		this.linkedLocation = linkedLocation;
	}

	@Override
	public boolean isRegularStudent() {
		return false;
	}

	@Override
	public void displayStudentInfo() {

		System.out.println("Mã sinh viên: " + super.id);
		System.out.println("Họ tên: " + super.fullName);
		System.out.println("Ngày tháng năm sinh: " + super.doB);
		System.out.println("Năm vào học: " + super.enrollmentYear);
		System.out.println("Điểm đầu vào: " + super.entranceExamScore);
		System.out.println("Course Name: " + super.courseName);
		System.out.println("Semester Name: " + super.semesterName);
		System.out.println("Gpa: " + super.gpa);
		System.out.println("linkedLocation: " + linkedLocation);
		System.out.println("************");
	}

	@Override
	public float calculateGpaBySemester(String semesterName) {
		// TODO Auto-generated method stub
		return 0;
	}

}
