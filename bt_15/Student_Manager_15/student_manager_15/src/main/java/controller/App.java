/**
 * @author ThangHT8
 * @date 25 Sept 2023
 * @version 1.0
 */

package controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import dao.StudentComparator;
import dao.StudentDAO;
import entities.Student;

public class App {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		while (true) {
			System.out.println("*****************************");
			System.out.println("Choose an option:");
			System.out.println("1. Add Part-time Student");
			System.out.println("2. Add Regular Student");
			System.out.println("3. View All Students");
			System.out.println("4. Check if a Student is Regular");
			System.out.println("5. Calculate GPA by Semester");
			System.out.println("6. Count Regular Students by Course");
			System.out.println("7. Find Top Entrance Exam Score by Course");
			System.out.println("8. View Linked Location Students by Course");
			System.out.println("9. View High GPA Students by Course");
			System.out.println("10. View Top GPA Student by Course");
			System.out.println("11. Sort Students by Type and Enrollment Year");
            System.out.println("12. Count Students by Enrollment Year");
			System.out.println("13. Exit");
			System.out.println("*****************************");

			System.out.print("Select: ");
			int choice = scanner.nextInt();
			scanner.nextLine();

			System.out.println("*****************************");

			switch (choice) {
			case 1:
				System.out.print("Full name: ");
				String fullName = scanner.nextLine();

				System.out.print("Date of birth (dd/MM/yyyy): ");
				String dobString = scanner.nextLine();
				Date dob = null;

				try {
					dob = new SimpleDateFormat("dd/MM/yyyy").parse(dobString);
				} catch (ParseException e) {
					System.out.println("Invalid date format. Please use dd/MM/yyyy.");
					continue;
				}

				System.out.print("Enrollment year: ");
				int enrollmentYear = scanner.nextInt();
				scanner.nextLine();

				System.out.print("Entrance exam score: ");
				float entranceExamScore = scanner.nextFloat();
				scanner.nextLine();

				System.out.print("Linked location: ");
				String linkedLocation = scanner.nextLine();

				System.out.print("Course Name: ");
				String courseName = scanner.nextLine();

				System.out.print("Semester Name: ");
				String semesterName = scanner.nextLine();

				System.out.print("Gpa: ");
				float gpa = scanner.nextFloat();
				scanner.nextLine();

				StudentDAO.insertPartTimeStudent(fullName, dob, enrollmentYear, entranceExamScore, linkedLocation,
						courseName, semesterName, gpa);
				break;

			case 2:
				System.out.print("Full name: ");
				fullName = scanner.nextLine();

				System.out.print("Date of birth (dd/MM/yyyy): ");
				dobString = scanner.nextLine();
				dob = null;

				try {
					dob = new SimpleDateFormat("dd/MM/yyyy").parse(dobString);
				} catch (ParseException e) {
					System.out.println("Invalid date format. Please use dd/MM/yyyy.");
					continue;
				}

				System.out.print("Enrollment year: ");
				enrollmentYear = scanner.nextInt();
				scanner.nextLine();

				System.out.print("Entrance exam score: ");
				entranceExamScore = scanner.nextFloat();
				scanner.nextLine();

				System.out.print("Course Name: ");
				courseName = scanner.nextLine();

				System.out.print("Semester Name: ");
				semesterName = scanner.nextLine();

				System.out.print("Gpa: ");
				gpa = scanner.nextFloat();
				scanner.nextLine();

				StudentDAO.insertRegularStudent(fullName, dob, enrollmentYear, entranceExamScore, courseName,
						semesterName, gpa);
				break;

			case 3:
				displayStudentList();
				break;

			case 4:
				checkRegularStudent(scanner);
				break;

			case 5:
				calculateGpaBySemester(scanner);
				break;

			case 6:
				countRegularStudentsByCourse(scanner);
				break;

			case 7:
				findTopEntranceExamScoreByCourse();
				break;

			case 8:
				System.out.print("Enter course name: ");
				courseName = scanner.nextLine();
				List<Student> linkedLocationStudents = StudentDAO.getLinkedLocationStudentsByCourse(courseName);
				displayStudents(linkedLocationStudents);
				break;

			case 9:
				System.out.print("Enter type (0 or 1): ");
				int type = scanner.nextInt();
				scanner.nextLine();

				List<Student> highGpaStudents = StudentDAO.getHighGpaStudentsByCourse(type);
				displayStudents(highGpaStudents);
				break;

			case 10:
				Map<String, Student> topGpaStudentsByDepartment = StudentDAO.findTopGpaStudentByCourse();
				for (Map.Entry<String, Student> entry : topGpaStudentsByDepartment.entrySet()) {
					courseName = entry.getKey();
					Student topGpaStudent = entry.getValue();
					System.out.println("Course Name: " + courseName);
					System.out.println("Top GPA Student:");
					topGpaStudent.displayStudentInfo();
				}
				break;

			case 11:
				List<Student> sortedStudents = StudentDAO.getAllStudents();
				Collections.sort(sortedStudents, new StudentComparator());
				displayStudents(sortedStudents);
				break;

			case 12:
				Map<Integer, Integer> enrollmentYearCounts = StudentDAO.countStudentsByEnrollmentYear();
				System.out.println("Enrollment Year Counts:");
				for (Map.Entry<Integer, Integer> entry : enrollmentYearCounts.entrySet()) {
					System.out.println(entry.getKey() + ": " + entry.getValue());
				}
				break;

			case 13:
				scanner.close();
				System.exit(0);

			default:
				System.out.println("Invalid option. Please try again.");
			}
		}
	}

	private static void displayStudents(List<Student> students) {
		if (students.isEmpty()) {
			System.out.println("No students found.");
		} else {
			for (Student student : students) {
				student.displayStudentInfo();
			}
		}
	}

	private static void displayStudentList() {
		List<Student> students = StudentDAO.getAllStudents();

		if (students.isEmpty()) {
			System.out.println("Student list is empty.");
		} else {
			System.out.println("Student list:");

			List<Student> partTimeStudent = new ArrayList<>();
			List<Student> regularStudent = new ArrayList<>();

			if (students.isEmpty()) {
				System.out.println("No students in the database.");
			} else {
				for (Student student : students) {
					Byte type = student.getType();

					if (type != null) {
						if (type == 0) {
							partTimeStudent.add(student);
						} else if (type == 1) {
							regularStudent.add(student);
						}
					}
				}
			}

			if (!partTimeStudent.isEmpty()) {
				System.out.println("------------------");
				System.out.println("Part Time Student: ");
				for (Student student : partTimeStudent) {
					student.displayStudentInfo();
				}
			}

			if (!regularStudent.isEmpty()) {
				System.out.println("------------------");
				System.out.println("Regular Student: ");
				for (Student student : regularStudent) {
					student.displayStudentInfo();
				}
			}
		}
	}

	private static void checkRegularStudent(Scanner scanner) {
		System.out.print("Enter student's ID: ");
		int studentId = scanner.nextInt();
		scanner.nextLine();

		boolean isRegular = StudentDAO.isRegularStudent(studentId);

		if (isRegular) {
			System.out.println("This student is a Regular Student.");
		} else {
			System.out.println("This student is not a Regular Student.");
		}
	}

	private static void calculateGpaBySemester(Scanner scanner) {
		System.out.print("Enter student's ID: ");
		int id = scanner.nextInt();
		scanner.nextLine();

		System.out.print("Enter semester name: ");
		String semesterName = scanner.nextLine();

		double gpa = StudentDAO.calculateGpaBySemester(id, semesterName);

		if (gpa >= 0) {
			System.out.println("GPA for " + semesterName + " is: " + gpa);
		} else {
			System.out.println("Unable to calculate GPA. Student or semester not found.");
		}
	}

	private static void countRegularStudentsByCourse(Scanner scanner) {
		System.out.print("Enter course name: ");
		String courseName = scanner.nextLine();

		int count = StudentDAO.countRegularStudentsByCourse(courseName);

		System.out.println("Number of Regular Students in " + courseName + ": " + count);
	}

	private static void findTopEntranceExamScoreByCourse() {
		Map<String, Student> topStudentsByCourse = StudentDAO.findTopEntranceExamScoreByCourse();

		if (!topStudentsByCourse.isEmpty()) {
			System.out.println("Top Entrance Exam Scores by CourseName:");

			for (Map.Entry<String, Student> entry : topStudentsByCourse.entrySet()) {
				String course = entry.getKey();
				Student topStudent = entry.getValue();

				System.out.println("Course: " + course);
				System.out.println("Top Student: ");
				topStudent.displayStudentInfo();
			}
		} else {
			System.out.println("No data available.");
		}
	}
}
