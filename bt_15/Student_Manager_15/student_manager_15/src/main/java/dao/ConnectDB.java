/**
 * @author ThangHT8
 * @date 20 Sept 2023
 * @version 1.0
 */

package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnectDB {

	// url, username, pass
	public static final String URL = "jdbc:sqlserver://localhost:1433;databaseName=StudentManager;"
			+ "encrypt=true;trustServerCertificate=true;sslProtocol=TLSv1.2";
	public static final String USER_NAME = "sa";
	public static final String PASSWORD = "123456";

	// tạo kết nối
	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			conn = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
//			System.out.println("Connect successful.");
		} catch (ClassNotFoundException | SQLException e) {
//			System.out.println("Connect fail.");
			e.printStackTrace();
		}

		return conn;
	}

	// Ngắt resultset, statement
	public static void closeRsStm(ResultSet rs, PreparedStatement stm) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (stm != null) {
			try {
				stm.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	// Ngắt kết nối
	public static void closeConnection(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
//				System.out.println("Ngắt Kết Nối Thành Công");
			} else {
//				System.out.println("Ngắt Kết Nối Thất Bại");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
