/**
 * @author ThangHT8
 * @date 25 Sept 2023
 * @version 1.0
 */

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entities.PartTimeStudent;
import entities.RegularStudent;
import entities.Student;

public class StudentDAO {

	public static void insertPartTimeStudent(String fullName, Date doB, Integer enrollmentYear, Float entranceExamScore,
			String linkedLocation, String courseName, String semesterName, Float gpa) {
		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQLQueries.INSERT_PART_TIME_STUDENT)) {

			preparedStatement.setByte(1, (byte) 0);
			preparedStatement.setString(2, fullName);
			preparedStatement.setDate(3, new java.sql.Date(doB.getTime()));
			preparedStatement.setInt(4, enrollmentYear);
			preparedStatement.setFloat(5, entranceExamScore);
			preparedStatement.setString(6, linkedLocation);
			preparedStatement.setString(7, courseName);
			preparedStatement.setString(8, semesterName);
			preparedStatement.setFloat(9, gpa);

			int rowsInserted = preparedStatement.executeUpdate();

			if (rowsInserted > 0) {
				System.out.println("Part-time student added successfully!");
			} else {
				System.out.println("Failed to add part-time student.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void insertRegularStudent(String fullName, Date doB, Integer enrollmentYear, Float entranceExamScore,
			String courseName, String semesterName, Float gpa) {
		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.INSERT_REGULAR_STUDENT)) {

			preparedStatement.setByte(1, (byte) 1);
			preparedStatement.setString(2, fullName);
			preparedStatement.setDate(3, new java.sql.Date(doB.getTime()));
			preparedStatement.setInt(4, enrollmentYear);
			preparedStatement.setFloat(5, entranceExamScore);
			preparedStatement.setString(6, courseName);
			preparedStatement.setString(7, semesterName);
			preparedStatement.setFloat(8, gpa);

			int rowsInserted = preparedStatement.executeUpdate();

			if (rowsInserted > 0) {
				System.out.println("Regular student added successfully!");
			} else {
				System.out.println("Failed to add regular student.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Student> getAllStudents() {
		List<Student> students = new ArrayList<>();
		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.SELECT_ALL_STUDENTS)) {
			ResultSet resultSet = preparedStatement.executeQuery();
			students = getStudentList(resultSet);
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Error: Failed to retrieve students from the database.");
		}
		return students;
	}

	private static List<Student> getStudentList(ResultSet rs) {
		List<Student> studentList = new ArrayList<>();
		try {
			while (rs.next()) {
				int type = rs.getInt("type");
				Student student;
				if (type == 1) {
					RegularStudent regularStudent = new RegularStudent();
					regularStudent.setId(rs.getInt("id"));
					regularStudent.setType((byte) type);
					regularStudent.setFullName(rs.getString("fullName"));
					regularStudent.setDoB(rs.getDate("doB"));
					regularStudent.setEnrollmentYear(rs.getInt("enrollmentYear"));
					regularStudent.setEntranceExamScore(rs.getFloat("entranceExamScore"));
					regularStudent.setCourseName(rs.getString("courseName"));
					regularStudent.setSemesterName(rs.getString("semesterName"));
					regularStudent.setGpa(rs.getFloat("gpa"));

					student = regularStudent;
				} else if (type == 0) {
					PartTimeStudent partTimeStudent = new PartTimeStudent();
					partTimeStudent.setId(rs.getInt("id"));
					partTimeStudent.setType((byte) type);
					partTimeStudent.setFullName(rs.getString("fullName"));
					partTimeStudent.setDoB(rs.getDate("doB"));
					partTimeStudent.setEnrollmentYear(rs.getInt("enrollmentYear"));
					partTimeStudent.setEntranceExamScore(rs.getFloat("entranceExamScore"));
					partTimeStudent.setLinkedLocation(rs.getString("linkedLocation"));
					partTimeStudent.setCourseName(rs.getString("courseName"));
					partTimeStudent.setSemesterName(rs.getString("semesterName"));
					partTimeStudent.setGpa(rs.getFloat("gpa"));

					student = partTimeStudent;
				} else {
					student = null;
				}

				studentList.add(student);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return studentList;
	}

	public static boolean isRegularStudent(int studentId) {
		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.CHECK_REGULAR_STUDENT)) {

			preparedStatement.setInt(1, studentId);
			ResultSet resultSet = preparedStatement.executeQuery();

			return resultSet.next();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static double calculateGpaBySemester(int studentId, String semesterName) {
		double gpa = -1.0;

		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQLQueries.CALCULATE_GPA_BY_SEMESTER)) {

			preparedStatement.setInt(1, studentId);
			preparedStatement.setString(2, semesterName);

			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				gpa = resultSet.getDouble("gpa");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return gpa;
	}

	public static int countRegularStudentsByCourse(String courseName) {
		List<Student> students = getAllStudents();
		int count = 0;
		for (Student student : students) {
			if (student.isRegularStudent() && courseName.equals(student.getCourseName())) {
				count++;
			}
		}
		return count;
	}

	public static Map<String, Student> findTopEntranceExamScoreByCourse() {
		List<Student> students = getAllStudents();
		Map<String, Student> topStudentsByCourse = new HashMap<>();

		for (Student student : students) {
			String courseName = student.getCourseName();

			if (!topStudentsByCourse.containsKey(courseName)
					|| student.getEntranceExamScore() > topStudentsByCourse.get(courseName).getEntranceExamScore()) {
				topStudentsByCourse.put(courseName, student);
			}
		}

		return topStudentsByCourse;
	}

	public static List<Student> getLinkedLocationStudentsByCourse(String courseName) {
		List<Student> students = new ArrayList<>();

		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQLQueries.SELECT_LINKED_LOCATION_STUDENTS)) {

			preparedStatement.setString(1, courseName);
			ResultSet resultSet = preparedStatement.executeQuery();
			students = getStudentList(resultSet);

		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Error: Failed to retrieve linked location students from the database.");
		}
		return students;
	}

	public static List<Student> getHighGpaStudentsByCourse(int type) {
		List<Student> students = new ArrayList<>();

		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(SQLQueries.SELECT_HIGH_GPA_STUDENTS)) {

			preparedStatement.setByte(1, (byte) type);
			preparedStatement.setByte(2, (byte) type);
			ResultSet resultSet = preparedStatement.executeQuery();
			students = getStudentList(resultSet);

		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Error: Failed to retrieve high GPA students from the database.");
		}
		return students;
	}

	public static Map<String, Student> findTopGpaStudentByCourse() {

		Map<String, Student> topStudentsByCourse = new HashMap<>();

		try (Connection connection = ConnectDB.getConnection(); Statement statement = connection.createStatement()) {

			ResultSet resultSet = statement.executeQuery(SQLQueries.SELECT_TOP_GPA_STUDENTS);

			while (resultSet.next()) {

				String courseName = resultSet.getString("courseName");
				int id = resultSet.getInt("id");
				double gpa = resultSet.getDouble("gpa");

				Student topStudent = getStudentById(id);

				if (topStudent != null && (!topStudentsByCourse.containsKey(courseName)
						|| gpa > topStudentsByCourse.get(courseName).getGpa())) {
					topStudentsByCourse.put(courseName, topStudent);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Error: Failed to retrieve top GPA students from the database.");
		}
		return topStudentsByCourse;
	}

	public static Student getStudentById(int studentId) {
		try (Connection connection = ConnectDB.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.SELECT_STUDENT_BY_ID)) {
			preparedStatement.setInt(1, studentId);
			ResultSet resultSet = preparedStatement.executeQuery();
			List<Student> students = getStudentList(resultSet);
			if (!students.isEmpty()) {
				return students.get(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Error: Failed to retrieve student by ID from the database.");
		}
		return null;
	}

	public static Map<Integer, Integer> countStudentsByEnrollmentYear() {
        Map<Integer, Integer> enrollmentYearCounts = new HashMap<>();

        List<Student> students = getAllStudents();
        for (Student student : students) {
            Integer enrollmentYear = student.getEnrollmentYear();
            enrollmentYearCounts.put(enrollmentYear, enrollmentYearCounts.getOrDefault(enrollmentYear, 0) + 1);
        }

        return enrollmentYearCounts;
    }

}
