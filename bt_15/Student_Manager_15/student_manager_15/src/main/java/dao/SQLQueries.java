/**
 * @author ThangHT8
 * @date 25 Sept 2023
 * @version 1.0
 */

package dao;

public class SQLQueries {

	public static final String INSERT_PART_TIME_STUDENT = "INSERT INTO Student (type, fullName, doB, enrollmentYear, entranceExamScore, linkedLocation, courseName, semesterName, gpa) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String INSERT_REGULAR_STUDENT = "INSERT INTO Student (type, fullName, doB, enrollmentYear, entranceExamScore, courseName, semesterName, gpa) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String SELECT_ALL_STUDENTS = "SELECT * FROM Student";
	public static final String CHECK_REGULAR_STUDENT = "SELECT 1 FROM Student WHERE id = ? AND type = 1";
	public static final String CALCULATE_GPA_BY_SEMESTER = "SELECT AVG(gpa) as gpa FROM Student WHERE id = ? AND semesterName = ?";
	public static final String SELECT_LINKED_LOCATION_STUDENTS = "SELECT * FROM Student WHERE courseName = ? AND linkedLocation IS NOT NULL";
	public static final String SELECT_HIGH_GPA_STUDENTS = 
	        "SELECT * FROM Student " +
	        "WHERE type = ? " +  
	        "AND semesterName = (SELECT MAX(semesterName) FROM Student WHERE type = ?) " +
	        "AND gpa >= 8.0";
	public static final String SELECT_TOP_GPA_STUDENTS = "SELECT courseName, id, MAX(gpa) AS gpa FROM Student GROUP BY courseName, id";
	public static final String SELECT_STUDENT_BY_ID = "SELECT * FROM Student WHERE id = ?";
	public static final String COUNT_STUDENTS_BY_ENROLLMENT_YEAR =
	        "SELECT enrollmentYear, COUNT(*) as count FROM Student WHERE courseName = ? GROUP BY enrollmentYear";

}
