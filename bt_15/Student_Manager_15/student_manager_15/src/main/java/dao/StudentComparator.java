/**
 * @author ThangHT8
 * @date 25 Sept 2023
 * @version 1.0
 */

package dao;

import java.util.Comparator;

import entities.Student;

public class StudentComparator implements Comparator<Student> {

	@Override
	public int compare(Student student1, Student student2) {
		
		int typeComparison = Byte.compare(student1.getType(), student2.getType());

		
		if (typeComparison == 0) {
			return Integer.compare(student2.getEnrollmentYear(), student1.getEnrollmentYear());
		}

		return typeComparison;
	}
}
