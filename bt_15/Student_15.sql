create database StudentManager;

create table Student(
	id  int  primary key  IDENTITY(1,1),
	type bit check (type in (0,1)),
	fullName	varchar(255) not null,
	doB		date,
	enrollmentYear   int,
	entranceExamScore 	float,
	linkedLocation varchar(255),
	courseName		varchar(255),
	semesterName  varchar(255),
	gpa		float
)