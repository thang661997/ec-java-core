/**
 * @author ThangHT8
 * @date 19 Sept 2023
 * @version 1.0
 */

package exception;

public class BirthDayException extends Exception{

	private static final long serialVersionUID = 1L;

	public BirthDayException(String message) {
		super(message);
	}
}
