/**
 * @author ThangHT8
 * @date 13 Sept 2023
 * @version 1.0
 */

package example;

import example.A.A1;

public class StringExample {

	public static void main(String[] args) {

		
		A a = new A();
		A1 a1 = a.new A1();
		a1.printa();
			

	}
	
}


class A{
	class A1{
		void printa() {
			System.out.println("A.A1");
		}
	}
}