/**
 * @author ThangHT8
 * @date 23 Sept 2023
 * @version 1.0
 */

package bt_java_4;

import java.util.ArrayList;
import java.util.List;

public class Household {

	private int numberOfMembers;
	private int houseNumber;
	private List<Person> familyMembers;

	public Household(int numberOfMembers, int houseNumber) {
		super();
		this.numberOfMembers = numberOfMembers;
		this.houseNumber = houseNumber;
		this.familyMembers = new ArrayList<>();
	}

	public void addPerson(Person person) {
		familyMembers.add(person);
	}

	public int getNumberOfMembers() {
		return numberOfMembers;
	}

	public void setNumberOfMembers(int numberOfMembers) {
		this.numberOfMembers = numberOfMembers;
	}

	public int getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	public List<Person> getFamilyMembers() {
		return familyMembers;
	}

	public void setFamilyMembers(List<Person> familyMembers) {
		this.familyMembers = familyMembers;
	}

}
