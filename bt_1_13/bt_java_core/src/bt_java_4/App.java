/**
 * @author ThangHT8
 * @date 23 Sept 2023
 * @version 1.0
 */

package bt_java_4;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Neighborhood neighborhood = new Neighborhood();

		while (true) {
			System.out.println("************************************");
			System.out.println("Welcome to the neighborhood management system");
			System.out.println("************************************");
			System.out.println("Please choose an option:");
			System.out.println("1. Enter household information");
			System.out.println("2. Display neighborhood information");
			System.out.println("3. Exit the program");
			System.out.println("************************************");

			int choice;
			System.out.print("Enter your choice: ");
			try {
				choice = sc.nextInt();
				sc.nextLine();
			} catch (NumberFormatException e) {
				System.out.println("Invalid choice. Please enter a valid option.");
				continue;
			}
			
			System.out.println("************************************");

			switch (choice) {
			case 1:
				enterHouseholdInformation(sc, neighborhood);
				break;

			case 2:
				neighborhood.displayNeighborhoodInfo();
				break;

			case 3:
				System.out.println("Exiting the program.");
				sc.close();
				System.exit(0);

			default:
				System.out.println("Invalid choice. Please enter a valid option.");
			}
		}
	}

	private static void enterHouseholdInformation(Scanner sc, Neighborhood neighborhood) {
		System.out.print("Enter the number of households to add: ");
		int n = sc.nextInt();
		sc.nextLine();

		for (int i = 0; i < n; i++) {
			System.out.println("Enter information for household number " + (i + 1) + ":");
			System.out.print("House number: ");
			int houseNumber = sc.nextInt();
			sc.nextLine();

			System.out.print("Number of family members: ");
			int numberOfNumbers = sc.nextInt();
			sc.nextLine();

			Household household = new Household(numberOfNumbers, houseNumber);

			for (int j = 0; j < numberOfNumbers; j++) {
				System.out.println("Enter information for family member number " + (j + 1) + ":");
				System.out.print("Full name: ");
				String fullName = sc.nextLine();

				System.out.print("Age: ");
				int age = sc.nextInt();
				sc.nextLine();

				System.out.print("Occupation: ");
				String occupation = sc.nextLine();

				System.out.print("ID card: ");
				String idCard = sc.nextLine();

				Person person = new Person(fullName, age, occupation, idCard);
				household.addPerson(person);
			}

			neighborhood.addHousehold(household);
		}
	}
}
