/**
 * @author ThangHT8
 * @date 23 Sept 2023
 * @version 1.0
 */

package bt_java_4;

public class Person {

	private String fullName;
	private int age;
	private String occupation;
	private String idCard;

	public Person(String fullName, int age, String occupation, String idCard) {
		super();
		this.fullName = fullName;
		this.age = age;
		this.occupation = occupation;
		this.idCard = idCard;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

}
