/**
 * @author ThangHT8
 * @date 23 Sept 2023
 * @version 1.0
 */

package bt_java_4;

import java.util.ArrayList;
import java.util.List;

public class Neighborhood {

	private List<Household> listOfHouseholds;

	public Neighborhood() {
		listOfHouseholds = new ArrayList<>();
	}

	public void addHousehold(Household household) {
		listOfHouseholds.add(household);
	}

	public void displayNeighborhoodInfo() {
		System.out.println("List of households in the neighborhood: ");
		for (Household household : listOfHouseholds) {
			System.out.println("House number: " + household.getHouseNumber());
			System.out.println("Number of family members: " + household.getFamilyMembers().size());
			System.out.println("List of family members: ");
			for (Person person : household.getFamilyMembers()) {
				System.out.println("Full name: " + person.getFullName());
				System.out.println("Age: " + person.getAge());
				System.out.println("Occupation: " + person.getOccupation());
				System.out.println("ID card: " + person.getIdCard());
				System.out.println("--------------");
			}
			System.out.println("----------------------------------------");
		}
	}
}
