/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_1;

import java.util.ArrayList;
import java.util.List;

public class StaffManagement {

	private List<Staff> staffList;

	public StaffManagement() {
		staffList = new ArrayList<>();
	}

	public void addNewStaff(Staff staff) {
		staffList.add(staff);
		System.out.println("Staff has been added.");
	}

	public void searchByName(String fullName) {
		boolean found = false;
		for (Staff staff : staffList) {
			if (staff.getFullName().equalsIgnoreCase(fullName)) {
				System.out.println("Staff information:");
				System.out.println("Full name: " + staff.getFullName());
				System.out.println("Age: " + staff.getAge());
				System.out.println("Gender: " + staff.getGender());
				System.out.println("Address: " + staff.getAddress());
				found = true;
				break;
			}
		}
		if (!found) {
			System.out.println("No staff found with full name: " + fullName);
		}
	}

	public void displayStaffList() {
		System.out.println("Staff list: ");
		for (Staff staff : staffList) {
			System.out.println("Full name: " + staff.getFullName());
			System.out.println("Age: " + staff.getAge());
			System.out.println("Gender: " + staff.getGender());
			System.out.println("Address: " + staff.getAddress());
			System.out.println("------------------------");
		}
	}

	public List<Staff> getStaffList() {
		return staffList;
	}

	public void setStaffList(List<Staff> staffList) {
		this.staffList = staffList;
	}

}
