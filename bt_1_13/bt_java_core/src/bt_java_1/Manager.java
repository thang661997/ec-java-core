/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_1;

import java.util.Scanner;

public class Manager {

	public static void main(String[] args) {
		StaffManagement staffManagement = new StaffManagement();
		Scanner sc = new Scanner(System.in);

		while (true) {
			System.out.println("******************************");
			System.out.println("1. Add new staff");
			System.out.println("2. Search by full name");
			System.out.println("3. Display staff list");
			System.out.println("4. Exit");
			System.out.println("******************************");
			System.out.print("Select: ");

			int choice = sc.nextInt();
			sc.nextLine();

			switch (choice) {
			case 1:
				System.out.print("Enter full name: ");
				String fullName = sc.nextLine();

				System.out.print("Enter age: ");
				int age = sc.nextInt();
				sc.nextLine();

				System.out.print("Enter gender: ");
				String gender = sc.nextLine();

				System.out.print("Enter address: ");
				String address = sc.nextLine();

				System.out.println("Select staff type:");
				System.out.println("1. Worker");
				System.out.println("2. Engineer");
				System.out.print("Select: ");

				int staffType = sc.nextInt();
				sc.nextLine();

				switch (staffType) {
				case 1:
					System.out.print("Enter worker level (1-10): ");
					int level = sc.nextInt();
					sc.nextLine();
					staffManagement.addNewStaff(new Worker(fullName, age, gender, address, level));
					break;

				case 2:
					System.out.print("Enter engineer major: ");
					String major = sc.nextLine();
					staffManagement.addNewStaff(new Engineer(fullName, age, gender, address, major));
					break;

				default:
					System.out.println("Invalid choice.");
				}
				break;

			case 2:
				System.out.print("Enter full name to search: ");
				String nameToSearch = sc.nextLine();
				staffManagement.searchByName(nameToSearch);
				break;

			case 3:
				staffManagement.displayStaffList();
				break;

			case 4:
				System.out.println("Program exited.");
				sc.close();
				System.exit(0);

			default:
				System.out.println("Invalid choice. Please select again.");
			}
		}
	}
}
