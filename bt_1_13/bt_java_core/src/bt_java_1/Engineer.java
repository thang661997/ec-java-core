/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_1;

public class Engineer extends Staff {

	private String major;

	public Engineer(String fullName, int age, String gender, String address, String major) {
		super(fullName, age, gender, address);
		this.major = major;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

}
