/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_10;

public class TextDocument {

	private String text;

	public TextDocument() {
		this.text = "";
	}

	public TextDocument(String text) {
		this.text = text;
	}

	public int countWords() {
		String[] words = text.trim().split("\\s+");
		return words.length;
	}

	public int countCharacterA() {
		int count = 0;
		String textLowerCase = text.toLowerCase();
		for (int i = 0; i < textLowerCase.length(); i++) {
			if (textLowerCase.charAt(i) == 'a') {
				count++;
			}
		}
		return count;
	}
	
	// so sánh chữ mã ASCII, không phải so sánh A, a

	public void normalizeText() {
		text = text.trim();
		text = text.replaceAll("\\s+", " ");
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
