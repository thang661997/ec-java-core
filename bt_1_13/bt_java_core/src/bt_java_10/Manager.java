/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_10;

import java.util.Scanner;

public class Manager {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		TextDocument document = new TextDocument();

		while (true) {
			System.out.println("******************************");
			System.out.println("1. Set text");
			System.out.println("2. Count words");
			System.out.println("3. Count character 'A'");
			System.out.println("4. Normalize text");
			System.out.println("5. Display text");
			System.out.println("6. Exit");
			System.out.println("******************************");
			System.out.print("Select: ");

			int choice = sc.nextInt();
			sc.nextLine();

			switch (choice) {
			case 1:
				System.out.print("Enter text: ");
				String inputText = sc.nextLine();
				document.setText(inputText);
				System.out.println("Text has been set.");
				break;

			case 2:
				int wordCount = document.countWords();
				System.out.println("Word count: " + wordCount);
				break;

			case 3:
				int aCount = document.countCharacterA();
				System.out.println("Count of 'A' characters: " + aCount);
				break;

			case 4:
				document.normalizeText();
				System.out.println("Text has been normalize.");
				break;

			case 5:
				System.out.println("Text: " + document.getText());
				break;

			case 6:
				System.out.println("Application is exiting.");
				sc.close();
				System.exit(0);

			default:
				System.out.println("Invalid selection. Please choose again.");
			}
		}
	}
}
