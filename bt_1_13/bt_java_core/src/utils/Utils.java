/**
 * @author ThangHT8
 * @date 19 Sept 2023
 * @version 1.0
 */

package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

	// chuyển đổi chuỗi thành Date
	public static Date parseDate(String dateStr) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			return dateFormat.parse(dateStr);
		} catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
	}
}
