/**
 * @author ThangHT8
 * @date 18 Sept 2023
 * @version 1.0
 */

package bt_java_12;

import java.util.ArrayList;
import java.util.List;

public class VehicleManagement {

	private List<Vehicle> vehicleList;

	public VehicleManagement() {
		vehicleList = new ArrayList<>();
	}
	
	public void addVehicle(Vehicle vehicle) {
		vehicleList.add(vehicle);
	}
	
	public void removeVehicle(String id) {
		vehicleList.removeIf(vehicle -> vehicle.getId().equals(id));
	}
}
