/**
 * @author ThangHT8
 * @date 18 Sept 2023
 * @version 1.0
 */

package bt_java_12;

public class Vehicle {

	private String id;
	private String manufacturer;
	private int manufacturingYear;
	private double price;
	private String color;

	public Vehicle() {
	}

	public Vehicle(String id, String manufacturer, int manufacturingYear, double price, String color) {
		super();
		this.id = id;
		this.manufacturer = manufacturer;
		this.manufacturingYear = manufacturingYear;
		this.price = price;
		this.color = color;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public int getManufacturingYear() {
		return manufacturingYear;
	}

	public void setManufacturingYear(int manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
