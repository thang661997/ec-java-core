/**
 * @author ThangHT8
 * @date 18 Sept 2023
 * @version 1.0
 */

package bt_java_12;

public class Truck extends Vehicle {

	private double payload;

	public Truck(String id, String manufacturer, int manufacturingYear, double price, String color, double payload) {
		super(id, manufacturer, manufacturingYear, price, color);
		this.payload = payload;
	}

	public double getPayload() {
		return payload;
	}

	public void setPayload(double payload) {
		this.payload = payload;
	}

}
