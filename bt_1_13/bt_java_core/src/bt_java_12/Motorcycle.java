/**
 * @author ThangHT8
 * @date 18 Sept 2023
 * @version 1.0
 */

package bt_java_12;

public class Motorcycle extends Vehicle {

	private double powerOutput;

	public Motorcycle(String id, String manufacturer, int manufacturingYear, double price, String color,
			double powerOutput) {
		super(id, manufacturer, manufacturingYear, price, color);
		this.powerOutput = powerOutput;
	}

	public double getPowerOutput() {
		return powerOutput;
	}

	public void setPowerOutput(double powerOutput) {
		this.powerOutput = powerOutput;
	}

}
