/**
 * @author ThangHT8
 * @date 18 Sept 2023
 * @version 1.0
 */

package bt_java_12;

public class Car extends Vehicle {

	private int numberOfSeats;
	private String engineType;

	public Car(String id, String manufacturer, int manufacturingYear, double price, String color, int numberOfSeats,
			String engineType) {
		super(id, manufacturer, manufacturingYear, price, color);
		this.numberOfSeats = numberOfSeats;
		this.engineType = engineType;
	}

	public int getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

}
