/**
 * @author ThangHT8
 * @date 23 Sept 2023
 * @version 1.0
 */

package bt_java_8;

import java.util.ArrayList;
import java.util.List;

public class Library {

	private List<BorrowCard> borrowingCards;

	public Library() {
		borrowingCards = new ArrayList<>();
	}

	public void addBorrowCard(BorrowCard borrowCard) {
		borrowingCards.add(borrowCard);
	}

	public void removeBorrowCard(int borrowingCode) {
		borrowingCards.removeIf(borrowCard -> borrowCard.getBorrowingCode() == borrowingCode);
	}
	
	public void displayBorrowCards() {
		System.out.println("List of borrowing cards: ");
		for(BorrowCard borrowCard : borrowingCards) {
			System.out.println(borrowCard.toString());
			System.out.println("---------------------------------------F");
		}
	}
}
