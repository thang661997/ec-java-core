/**
 * @author ThangHT8
 * @date 23 Sept 2023
 * @version 1.0
 */

package bt_java_8;

public class BorrowCard {

	private int borrowingCode;
	private int borrowingDate;
	private int dueDate;
	private int bookCode;
	private Student borrowingStudent;

	public BorrowCard(int borrowingCode, int borrowingDate, int dueDate, int bookCode, Student borrowingStudent) {
		super();
		this.borrowingCode = borrowingCode;
		this.borrowingDate = borrowingDate;
		this.dueDate = dueDate;
		this.bookCode = bookCode;
		this.borrowingStudent = borrowingStudent;
	}

	public int getBorrowingCode() {
		return borrowingCode;
	}

	public void setBorrowingCode(int borrowingCode) {
		this.borrowingCode = borrowingCode;
	}

	public int getBorrowingDate() {
		return borrowingDate;
	}

	public void setBorrowingDate(int borrowingDate) {
		this.borrowingDate = borrowingDate;
	}

	public int getDueDate() {
		return dueDate;
	}

	public void setDueDate(int dueDate) {
		this.dueDate = dueDate;
	}

	public int getBookCode() {
		return bookCode;
	}

	public void setBookCode(int bookCode) {
		this.bookCode = bookCode;
	}

	public Student getBorrowingStudent() {
		return borrowingStudent;
	}

	public void setBorrowingStudent(Student borrowingStudent) {
		this.borrowingStudent = borrowingStudent;
	}

	public String toString() {
		return "Borrowing Code: " + borrowingCode + ", Borrowing Date: " + borrowingDate + ", Due Date: " + dueDate
				+ ", Book Code: " + bookCode + "\n" + borrowingStudent.toString();
	}

}
