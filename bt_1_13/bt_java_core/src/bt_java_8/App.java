/**
 * @author ThangHT8
 * @date 24 Sept 2023
 * @version 1.0
 */

package bt_java_8;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Library library = new Library();
		int borrowingCode = 1;

		while (true) {
			System.out.println("*******************************************");
			System.out.println("Welcome to the National University Library");
			System.out.println("*******************************************");
			System.out.println("Please choose an option:");
			System.out.println("1. Add a borrowing card");
			System.out.println("2. Remove a borrowing card by borrowing code");
			System.out.println("3. Display the list of borrowing cards");
			System.out.println("4. Exit");
			System.out.println("*******************************************");

			int choice;
			System.out.print("Enter your choice: ");
			try {
				choice = sc.nextInt();
				sc.nextLine();
			} catch (NumberFormatException e) {
				System.out.println("Invalid choice. Please try again.");
				continue;
			}

			System.out.println("*******************************************");

			switch (choice) {
			case 1:
				addBorrowCard(sc, library, borrowingCode);
				borrowingCode++;
				break;

			case 2:
                System.out.print("Enter the borrowing code to remove: ");
                int codeToRemove = sc.nextInt();
                sc.nextLine();
                library.removeBorrowCard(codeToRemove);
                System.out.println("The borrowing card has been removed.");
                break;
                
            case 3:
                library.displayBorrowCards();
                break;
                
            case 4:
                System.out.println("Exiting the program.");
                sc.close();
                System.exit(0);
                
            default:
                System.out.println("Invalid choice. Please try again.");
			}
		}
	}

	private static void addBorrowCard(Scanner sc, Library library, int borrowingCode) {
		System.out.println("Enter borrowing card information:");
		System.out.print("Borrowing Date: ");
		int borrowingDate = sc.nextInt();
		sc.nextLine();
		System.out.print("Due Date: ");
		int dueDate = sc.nextInt();
		sc.nextLine();
		System.out.print("Book Code: ");
		int bookCode = sc.nextInt();
		sc.nextLine();

		System.out.println("Enter student information:");
		System.out.print("Full Name: ");
		String fullName = sc.nextLine();
		System.out.print("Age: ");
		int age = sc.nextInt();
		sc.nextLine();
		System.out.print("Class: ");
		String className = sc.nextLine();

		Student student = new Student(fullName, age, className);
		BorrowCard borrowCard = new BorrowCard(borrowingCode, borrowingDate, dueDate, bookCode, student);
		library.addBorrowCard(borrowCard);
		System.out.println("The borrowing card has been added to the list.");
	}
}
