/**
 * @author ThangHT8
 * @date 13 Sept 2023
 * @version 1.0
 */

package bt_java_5;

public class Room {

	private RoomDetails romDetails;
	private Person person;

	public Room(RoomDetails romDetails) {
		super();
		this.romDetails = romDetails;
	}

	public RoomDetails getRomDetails() {
		return romDetails;
	}

	public void setRomDetails(RoomDetails romDetails) {
		this.romDetails = romDetails;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
