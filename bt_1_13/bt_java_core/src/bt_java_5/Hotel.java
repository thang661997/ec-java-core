/**
 * @author ThangHT8
 * @date 13 Sept 2023
 * @version 1.0
 */

package bt_java_5;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

	private List<Room> rooms;

	public Hotel() {
		rooms = new ArrayList<>();
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public void addRoom(Room room) {
		rooms.add(room);
	}

	public void removeRoom(String idCardNumber) {
		for (Room room : rooms) {
			if (room.getPerson() != null && room.getPerson().getIdCardNumber().equals(idCardNumber)) {
				rooms.remove(room);
				System.out.println("Khách hàng đã bị xóa khỏi phòng.");
				return;
			}
		}
		System.out.println("Không tìm thấy khách hàng với số chứng minh nhân dân: " + idCardNumber);
	}

	public double calculateBill(String idCardNumber, int numberOfDays) {
		double totalBill = 0.0;

		for (Room room : rooms) {
			if (room.getPerson() != null && room.getPerson().getIdCardNumber().equals(idCardNumber)) {
				totalBill += numberOfDays * room.getRomDetails().getRoomPrice();
			}
		}

		return totalBill;
	}

	public void displayRoomList() {
		if (rooms.isEmpty()) {
			System.out.println("Không có phòng nào trong khách sạn.");
		} else {
			System.out.println("Danh sách phòng và thông tin khách hàng: ");
			for (int i = 0; i < rooms.size(); i++) {
				Room room = rooms.get(i);
				System.out.println("Phòng " + (i + 1) + ":");
				System.out.println("Loại phòng: " + room.getRomDetails().getRoomType());
				if (room.getPerson() != null) {
					System.out.println("Khách hàng: " + room.getPerson().getName());
					System.out.println("Số CMND: " + room.getPerson().getIdCardNumber());
				} else {
					System.out.println("Không có khách hàng.");
				}
				System.out.println("---------------------");
			}
		}
	}
}
