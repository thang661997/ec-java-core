/**
 * @author ThangHT8
 * @date 13 Sept 2023
 * @version 1.0
 */

package bt_java_5;

import java.util.Scanner;

public class HotelManager {

    public static void main(String[] args) {
        Hotel hotel = new Hotel();

        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("******************************");
            System.out.println("1. Thêm khách hàng");
            System.out.println("2. Xóa khách hàng bằng số CMND");
            System.out.println("3. Tính tiền thuê phòng cho khách hàng");
            System.out.println("4. Hiển thị danh sách phòng và khách hàng");
            System.out.println("5. Thoát");
            System.out.println("******************************");
            System.out.print("Chọn: ");

            int choice = sc.nextInt();
            sc.nextLine();
            System.out.println("******************************");

            switch (choice) {
                case 1:
                    System.out.print("Chọn loại phòng (A, B hoặc C): ");
                    String roomType = sc.nextLine();
                    double roomPrice;

                    switch (roomType.toUpperCase()) {
                        case "A":
                            roomPrice = 500.0;
                            break;
                        case "B":
                            roomPrice = 300.0;
                            break;
                        case "C":
                            roomPrice = 100.0;
                            break;
                        default:
                            System.out.println("Loại phòng không hợp lệ.");
                            continue;
                    }

                    RoomDetails roomDetails = new RoomDetails(roomType, roomPrice);
                    Room room = new Room(roomDetails);

                    System.out.print("Nhập tên khách hàng: ");
                    String name = sc.nextLine();
                    System.out.print("Nhập tuổi: ");
                    int age = sc.nextInt();
                    sc.nextLine();
                    System.out.print("Nhập số chứng minh nhân dân: ");
                    String idCardNumber = sc.nextLine();
                    Person person = new Person(name, age, idCardNumber);

                    room.setPerson(person);
                    hotel.addRoom(room);

                    System.out.println("Khách hàng đã được thêm vào phòng.");
                    break;

                case 2:
                    System.out.print("Nhập số chứng minh nhân dân để xóa khách hàng: ");
                    String idToDelete = sc.nextLine();
                    hotel.removeRoom(idToDelete);
                    break;

                case 3:
                    System.out.print("Nhập số chứng minh nhân dân để tính tiền thuê phòng: ");
                    String idToCalculate = sc.nextLine();
                    System.out.print("Nhập số ngày thuê: ");
                    int numberOfDays = sc.nextInt();
                    sc.nextLine();

                    double bill = hotel.calculateBill(idToCalculate, numberOfDays);
                    System.out.println("Tổng tiền thuê phòng: $" + bill);
                    break;

                case 4:
                    hotel.displayRoomList();
                    break;

                case 5:
                    System.out.println("Thoát.");
                    sc.close();
                    System.exit(0);

                default:
                    System.out.println("Lựa chọn không hợp lệ.");
            }
        }
    }
}

