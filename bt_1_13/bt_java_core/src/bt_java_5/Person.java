/**
 * @author ThangHT8
 * @date 13 Sept 2023
 * @version 1.0
 */

package bt_java_5;

public class Person {

	private String name;
	private int age;
	private String idCardNumber;

	public Person() {
		super();
	}

	public Person(String name, int age, String idCardNumber) {
		super();
		this.name = name;
		this.age = age;
		this.idCardNumber = idCardNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getIdCardNumber() {
		return idCardNumber;
	}

	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber;
	}

}
