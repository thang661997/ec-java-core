/**
 * @author ThangHT8
 * @date 13 Sept 2023
 * @version 1.0
 */

package bt_java_5;

public class RoomDetails {

	private String roomType;
	private double roomPrice;

	public RoomDetails() {
		super();
	}

	public RoomDetails(String roomType, double roomPrice) {
		super();
		this.roomType = roomType;
		this.roomPrice = roomPrice;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public double getRoomPrice() {
		return roomPrice;
	}

	public void setRoomPrice(double roomPrice) {
		this.roomPrice = roomPrice;
	}

}
