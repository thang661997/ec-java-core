/**
 * @author ThangHT8
 * @date 18 Sept 2023
 * @version 1.0
 */

package bt_java_11;

import java.util.Scanner;

public class ComplexNumber {

	private double realPart;
	private double imaginaryPart;

	public ComplexNumber() {
		this.realPart = 0;
		this.imaginaryPart = 0;
	}

	public ComplexNumber(double realPart, double imaginaryPart) {
		this.realPart = realPart;
		this.imaginaryPart = imaginaryPart;
	}
	
	public void inputComplexNumber(Scanner sc) {		
		System.out.print("Enter the real part: ");
		this.realPart = sc.nextDouble();
		
		System.out.print("Enter the imaginary part: ");
		this.imaginaryPart = sc.nextDouble();
	}
	
	public void displayComplexNumber() {
		System.out.println("Complex number: " + this.realPart + " + " + this.imaginaryPart + "i");
	}
	
	public ComplexNumber add(ComplexNumber other) {
		double newRealPart = this.realPart + other.realPart;
		double newImaginaryPart = this.imaginaryPart + other.imaginaryPart;
		return new ComplexNumber(newRealPart, newImaginaryPart);
	}
	
	public ComplexNumber multiply(ComplexNumber other) {
		double newRealPart = this.realPart * other.realPart - this.imaginaryPart * other.imaginaryPart;
		double newImaginaryPart = this.realPart * other.imaginaryPart + this.imaginaryPart * other.realPart;
		return new ComplexNumber(newRealPart, newImaginaryPart);
	}
	
	

	public double getRealPart() {
		return realPart;
	}

	public void setRealPart(double realPart) {
		this.realPart = realPart;
	}

	public double getImaginaryPart() {
		return imaginaryPart;
	}

	public void setImaginaryPart(double imaginaryPart) {
		this.imaginaryPart = imaginaryPart;
	}
	
	

}
