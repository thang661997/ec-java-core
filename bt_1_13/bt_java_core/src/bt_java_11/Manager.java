/**
 * @author ThangHT8
 * @date 18 Sept 2023
 * @version 1.0
 */

package bt_java_11;

import java.util.Scanner;

public class Manager {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the first complex number: ");
		ComplexNumber complexNumber1 = new ComplexNumber();
		complexNumber1.inputComplexNumber(sc);
		
		System.out.println("Enter the second complex number: ");
		ComplexNumber complexNumber2 = new ComplexNumber();
		complexNumber2.inputComplexNumber(sc);
		
		System.out.println("First complex number: ");
		complexNumber1.displayComplexNumber();
		
		System.out.println("Second complex number:");
        complexNumber2.displayComplexNumber();

        System.out.println("Choose an operation:");
        System.out.println("1. Addition");
        System.out.println("2. Multiplication");

        int choice = sc.nextInt();

        switch (choice) {
            case 1:
                ComplexNumber sum = complexNumber1.add(complexNumber2);
                System.out.println("Sum:");
                sum.displayComplexNumber();
                break;

            case 2:
                ComplexNumber product = complexNumber1.multiply(complexNumber2);
                System.out.println("Product:");
                product.displayComplexNumber();
                break;

            default:
                System.out.println("Invalid choice.");
        }
	}
}
