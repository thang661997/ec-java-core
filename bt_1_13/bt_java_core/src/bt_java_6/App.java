/**
 * @author ThangHT8
 * @date 23 Sept 2023
 * @version 1.0
 */

package bt_java_6;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ClassManager classManager = new ClassManager();

		while (true) {
			System.out.println("************************************");
			System.out.println("Welcome to the High School Student Management Application");
			System.out.println("************************************");
			System.out.println("Please choose an option:");
			System.out.println("1. Add a new student");
			System.out.println("2. Display students aged 20");
			System.out.println("3. Count the number of students aged 23 from DN");
			System.out.println("4. Exit");
			System.out.println("************************************");

			int choice;
			System.out.print("Enter your choice: ");
			try {
				choice = sc.nextInt();
				sc.nextLine();
			} catch (NumberFormatException e) {
				System.out.println("Invalid choice. Please try again.");
				continue;
			}

			System.out.println("************************************");

			switch (choice) {
			case 1:
				addNewStudent(sc, classManager);
				break;

			case 2:
				classManager.displayStudentsAged20();
				break;

			case 3:
				int count = classManager.countStudentsAged23FromDN();
				System.out.println("Number of students aged 23 from DN: " + count);
				break;

			case 4:
				System.out.println("Exiting the program.");
				sc.close();
				System.exit(0);

			default:
				System.out.println("Invalid choice. Please try again.");
			}
		}
	}

	private static void addNewStudent(Scanner sc, ClassManager classManager) {
		System.out.print("Enter the number of students to add: ");
		int n = sc.nextInt();
		sc.nextLine();

		for (int i = 0; i < n; i++) {
			System.out.println("Enter information for student " + (i + 1) + ":");
			System.out.print("Full Name: ");
			String fullName = sc.nextLine();

			System.out.print("Age ");
			int age = sc.nextInt();
			sc.nextLine();

			System.out.print("Hometown: ");
			String hometown = sc.nextLine();

			Student student = new Student(fullName, age, hometown);
			classManager.addStudent(student);
		}
	}
}
