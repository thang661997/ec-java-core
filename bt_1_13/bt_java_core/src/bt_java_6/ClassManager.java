/**
 * @author ThangHT8
 * @date 23 Sept 2023
 * @version 1.0
 */

package bt_java_6;

import java.util.ArrayList;
import java.util.List;

public class ClassManager {

	private List<Student> studentList;

	public ClassManager() {
		studentList = new ArrayList<>();
	}

	public void addStudent(Student student) {
		studentList.add(student);
	}

	public void displayStudentsAged20() {
		System.out.println("List of students aged 20: ");
		for (Student student : studentList) {
			if (student.getAge() == 20) {
				System.out.println(student.toString());
			}
		}
	}

	public int countStudentsAged23FromDN() {
		int count = 0;
		for (Student student : studentList) {
			if (student.getAge() == 23 && student.getHometown().equalsIgnoreCase("DN")) {
				count++;
			}
		}
		return count;
	}
}
