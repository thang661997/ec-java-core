/**
 * @author ThangHT8
 * @date 23 Sept 2023
 * @version 1.0
 */

package bt_java_6;

public class Student {

	private String fullName;
	private int age;
	private String hometown;

	public Student(String fullName, int age, String hometown) {
		super();
		this.fullName = fullName;
		this.age = age;
		this.hometown = hometown;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getHometown() {
		return hometown;
	}

	public void setHometown(String hometown) {
		this.hometown = hometown;
	}

	@Override
	public String toString() {
		return "Student [fullName=" + fullName + ", age=" + age + ", hometown=" + hometown + "]";
	}

}
