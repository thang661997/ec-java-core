/**
 * @author ThangHT8
 * @date 15 Sept 2023
 * @version 1.0
 */

package bt_java_9;

import java.util.Scanner;

public class Customer {

	private String fullNameOfHouseholdHead;
	private String houseNumber;
	private int electricityMeterCode;

	public Customer(String fullNameOfHouseholdHead, String houseNumber, int electricityMeterCode) {
		super();
		this.fullNameOfHouseholdHead = fullNameOfHouseholdHead;
		this.houseNumber = houseNumber;
		this.electricityMeterCode = electricityMeterCode;
	}

	public static Customer createCustomer(Scanner scanner) {
		System.out.print("Enter full name of household head: ");
		String fullNameOfHouseholdHead = scanner.nextLine();

		System.out.print("Enter house number: ");
		String houseNumber = scanner.nextLine();

		System.out.print("Enter electricity meter code: ");
		int electricityMeterCode = scanner.nextInt();
		scanner.nextLine();

		return new Customer(fullNameOfHouseholdHead, houseNumber, electricityMeterCode);
	}

	public String getFullNameOfHouseholdHead() {
		return fullNameOfHouseholdHead;
	}

	public void setFullNameOfHouseholdHead(String fullNameOfHouseholdHead) {
		this.fullNameOfHouseholdHead = fullNameOfHouseholdHead;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public int getElectricityMeterCode() {
		return electricityMeterCode;
	}

	public void setElectricityMeterCode(int electricityMeterCode) {
		this.electricityMeterCode = electricityMeterCode;
	}

	public void displayCutomer() {
		System.out.println("Customer [fullNameOfHouseholdHead=" + fullNameOfHouseholdHead + ", houseNumber=" + houseNumber
				+ ", electricityMeterCode=" + electricityMeterCode + "]");
	}

}
