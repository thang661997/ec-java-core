/**
 * @author ThangHT8
 * @date 15 Sept 2023
 * @version 1.0
 */

package bt_java_9;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Manager {

	public static void main(String[] args) {
		List<Customer> customers = new ArrayList<>();
		List<Receipt> receipts = new ArrayList<>();

		Scanner sc = new Scanner(System.in);

		while (true) {
			System.out.println("****************************");
			System.out.println("1. Add customer. ");
			System.out.println("2. Edit customer. ");
			System.out.println("3. Delete customer. ");
			System.out.println("4. Display customer information. ");
			System.out.println("5. Display receipt information. ");
			System.out.println("6. Exit");
			System.out.println("****************************");
			System.out.print("Select: ");

			int choice = sc.nextInt();
			sc.nextLine();

			System.out.println("****************************");

			switch (choice) {
			case 1:
				System.out.print("Enter customer's full name: ");
				String fullNameOfHouseholdHead = sc.nextLine();
				System.out.print("Enter house number: ");
				String houseNumber = sc.nextLine();
				System.out.print("Enter electricity meter code: ");
				int electricityMeterCode = sc.nextInt();
				sc.nextLine();

				Customer customer = new Customer(fullNameOfHouseholdHead, houseNumber, electricityMeterCode);
				customers.add(customer);
				Receipt receipt = new Receipt(customer, 0, 0);
				receipts.add(receipt);
				System.out.println("Customer has been added.");
				break;

			case 2:
				System.out.print("Enter electricity meter code to edit: ");
				int electricityMeterCodeEdit = sc.nextInt();
				sc.nextLine();
				boolean foundCustomerToEdit = false;

				for (Customer c : customers) {
					if (Integer.valueOf(c.getElectricityMeterCode())
							.equals(Integer.valueOf(electricityMeterCodeEdit))) {
						System.out.print("Enter new house number: ");
						c.setHouseNumber(sc.nextLine());
						System.out.println("Customer information updated.");
						foundCustomerToEdit = true;
						break;
					}
				}
				if (!foundCustomerToEdit) {
					System.out.println("No customer found with electric meter code: " + electricityMeterCodeEdit);
				}
				break;

			case 3:
				System.out.print("Enter customer's electric meter code to delete: ");
				int electricityMeterCodeDelete = sc.nextInt();
				sc.nextLine();
				boolean foundCustomerToDeledte = false;

				for (Customer c : customers) {
					if (Integer.valueOf(c.getElectricityMeterCode())
							.equals(Integer.valueOf(electricityMeterCodeDelete))) {
						customers.remove(c);
						System.out.println("Customer has been deleted.");
						foundCustomerToDeledte = true;
						break;
					}
				}
				if (!foundCustomerToDeledte) {
					System.out.println("No customer found with electric meter code: " + electricityMeterCodeDelete);
				}
				break;

			case 4:
				System.out.println("Customer information: ");
				System.out.println("---------------------");
				for (Customer c : customers) {
					c.displayCutomer();
					System.out.println("---------------------");
				}
				break;

			case 5:
				System.out.print("Enter customer's electric meter code: ");
				int electricityMeterCodeToDisplay = sc.nextInt();
				sc.nextLine();
				boolean foundReceipt = false;

				for (Receipt rc : receipts) {
					if (Integer.valueOf(rc.getCustomer().getElectricityMeterCode())
							.equals(Integer.valueOf(electricityMeterCodeToDisplay))) {
						
						int oldElectricityIndex, newElectricityIndex;

						do {
							System.out.print("Enter old electric reading: ");
							oldElectricityIndex = sc.nextInt();
							System.out.print("Enter new electric reading (must be greater than old reading): ");
							newElectricityIndex = sc.nextInt();
							sc.nextLine();

							if (newElectricityIndex <= oldElectricityIndex) {
								System.out.println("New reading must be greater than old reading. Please try again.");
							}
						} while (newElectricityIndex <= oldElectricityIndex);

						rc.setOldElectricityIndex(oldElectricityIndex);
						rc.setNewElectricityIndex(newElectricityIndex);
						rc.setPaymentAmount(rc.electricityBill(oldElectricityIndex, newElectricityIndex));

						rc.displayReceipt();

						foundReceipt = true;
						break;
					}
				}
				if (!foundReceipt) {
					System.out.println("No receipt found for electric meter code: " + electricityMeterCodeToDisplay);
				}
				break;

			case 6:
				System.out.println("Application ended. ");
				sc.close();
				System.exit(0);

			default:
				System.out.println("Invalid selection. ");
			}
		}
	}
}
