/**
 * @author ThangHT8
 * @date 15 Sept 2023
 * @version 1.0
 */

package bt_java_9;

public class Receipt {

	private Customer customer;
	private int oldElectricityIndex;
	private int newElectricityIndex;
	private int paymentAmount;

	public Receipt(Customer customer, int oldElectricityIndex, int newElectricityIndex) {
		this.customer = customer;
		this.oldElectricityIndex = oldElectricityIndex;
		this.newElectricityIndex = newElectricityIndex;
		this.paymentAmount = electricityBill(oldElectricityIndex, newElectricityIndex);
	}

	public int electricityBill(int oldElectricityIndex, int newElectricityIndex) {
		return (newElectricityIndex - oldElectricityIndex) * 5;
	}
	
	public void displayReceipt() {
		System.out.println("Receipt [fullNameOfHouseholdHead=" + customer.getFullNameOfHouseholdHead() + ", houseNumber=" + customer.getHouseNumber()
				+ ", electricityMeterCode=" + customer.getElectricityMeterCode() + ", oldElectricityIndex=" + oldElectricityIndex
				+ ", newElectricityIndex=" + newElectricityIndex + ", paymentAmount=" + paymentAmount + "]");
	}

	public int getOldElectricityIndex() {
		return oldElectricityIndex;
	}

	public void setOldElectricityIndex(int oldElectricityIndex) {
		this.oldElectricityIndex = oldElectricityIndex;
	}

	public int getNewElectricityIndex() {
		return newElectricityIndex;
	}

	public void setNewElectricityIndex(int newElectricityIndex) {
		this.newElectricityIndex = newElectricityIndex;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(int paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

}
