/**
 * @author ThangHT8
 * @date 19 Sept 2023
 * @version 1.0
 */

package bt_java_13;

import java.util.ArrayList;
import java.util.Date;

public abstract class Employee {

	protected int id;
	protected String fullName;
	protected Date birthDay;
	protected String phone;
	protected String email;
	protected int employeeType; // 0: Experience, 1: Fresher, 2: Intern
	protected static int employeeCount = 0;
	protected ArrayList<Certificate> certificates;

	public Employee(int id, String fullName, Date birthDay, String phone, String email, int employeeType) {
		this.id = id;
		this.fullName = fullName;
		this.birthDay = birthDay;
		this.phone = phone;
		this.email = email;
		this.employeeType = employeeType;
		this.certificates = new ArrayList<>();
		employeeCount++;
	}

	// hiển thị thông tin nhân viên
	public abstract void showInfo();

	// thêm bằng cấp
	public void addCertificate(Certificate certificate) {
		certificates.add(certificate);
	}

	// lấy số lượng nhân viên
	public static int getEmployeeCount() {
		return employeeCount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(int employeeType) {
		this.employeeType = employeeType;
	}

	public ArrayList<Certificate> getCertificates() {
		return certificates;
	}

	public void setCertificates(ArrayList<Certificate> certificates) {
		this.certificates = certificates;
	}

	public static void setEmployeeCount(int employeeCount) {
		Employee.employeeCount = employeeCount;
	}

}
