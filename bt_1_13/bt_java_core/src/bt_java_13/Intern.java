/**
 * @author ThangHT8
 * @date 19 Sept 2023
 * @version 1.0
 */

package bt_java_13;

import java.util.Date;

public class Intern extends Employee {

	private String majors;
	private String semester;
	private String universityName;

	public Intern(int id, String fullName, Date birthDay, String phone, String email, int employeeType, String majors,
			String semester, String universityName) {
		super(id, fullName, birthDay, phone, email, 2);
		this.majors = majors;
		this.semester = semester;
		this.universityName = universityName;
	}

	@Override
	public void showInfo() {
		System.out.println("Experience Employee: ");
		System.out.println("ID: " + super.id);
		System.out.println("Full Name: " + super.fullName);
		System.out.println("Birth Day: " + super.birthDay);
		System.out.println("Phone: " + super.phone);
		System.out.println("Email: " + super.email);
		System.out.println("Employee Type: " + super.employeeType);
		System.out.println("Majors: " + majors);
		System.out.println("Semester: " + semester);
		System.out.println("University Name: " + universityName);
	}

	public String getMajors() {
		return majors;
	}

	public void setMajors(String majors) {
		this.majors = majors;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

}
