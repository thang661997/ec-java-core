/**
 * @author ThangHT8
 * @date 19 Sept 2023
 * @version 1.0
 */

package bt_java_13;

import java.util.Date;

public class Experience extends Employee {

	private int expInYear;
	private String proSkill;

	public Experience(int id, String fullName, Date birthDay, String phone, String email, int employeeType,
			int expInYear, String proSkill) {
		super(id, fullName, birthDay, phone, email, 0);
		this.expInYear = expInYear;
		this.proSkill = proSkill;
	}

	@Override
	public void showInfo() {
		System.out.println("Experience Employee: ");
		System.out.println("ID: " + super.id);
		System.out.println("Full Name: " + super.fullName);
		System.out.println("Birth Day: " + super.birthDay);
		System.out.println("Phone: " + super.phone);
		System.out.println("Email: " + super.email);
		System.out.println("Employee Type: " + super.employeeType);
		System.out.println("Experience in years: " + expInYear);
		System.out.println("Professional Skill: " + proSkill);
		System.out.println("------------------------");
	}

	public int getExpInYear() {
		return expInYear;
	}

	public void setExpInYear(int expInYear) {
		this.expInYear = expInYear;
	}

	public String getProSkill() {
		return proSkill;
	}

	public void setProSkill(String proSkill) {
		this.proSkill = proSkill;
	}

}
