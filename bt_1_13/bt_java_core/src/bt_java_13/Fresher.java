/**
 * @author ThangHT8
 * @date 19 Sept 2023
 * @version 1.0
 */

package bt_java_13;

import java.util.Date;

public class Fresher extends Employee {

	private Date graduationDate;
	private String graduationRank;
	private String education;

	public Fresher(int id, String fullName, Date birthDay, String phone, String email, int employeeType,
			Date graduationDate, String graduationRank, String education) {
		super(id, fullName, birthDay, phone, email, 1);
		this.graduationDate = graduationDate;
		this.graduationRank = graduationRank;
		this.education = education;
	}

	@Override
	public void showInfo() {
		System.out.println("Experience Employee: ");
		System.out.println("ID: " + super.id);
		System.out.println("Full Name: " + super.fullName);
		System.out.println("Birth Day: " + super.birthDay);
		System.out.println("Phone: " + super.phone);
		System.out.println("Email: " + super.email);
		System.out.println("Employee Type: " + super.employeeType);
		System.out.println("Graduation Date: " + graduationDate);
		System.out.println("Graduation Rank: " + graduationRank);
		System.out.println("Education: " + education);
	}

	public Date getGraduationDate() {
		return graduationDate;
	}

	public void setGraduationDate(Date graduationDate) {
		this.graduationDate = graduationDate;
	}

	public String getGraduationRank() {
		return graduationRank;
	}

	public void setGraduationRank(String graduationRank) {
		this.graduationRank = graduationRank;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

}
