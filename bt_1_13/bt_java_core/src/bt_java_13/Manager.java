/**
 * @author ThangHT8
 * @date 19 Sept 2023
 * @version 1.0
 */

package bt_java_13;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import exception.BirthDayException;
import exception.EmailException;
import exception.PhoneException;
import utils.Utils;

public class Manager {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Employee> employees = new ArrayList<>();

		while (true) {
			System.out.println("****************************");
			System.out.println("1. Add new employee");
			System.out.println("2. Edit employee");
			System.out.println("3. Delete employee");
			System.out.println("4. Show all employees");
			System.out.println("5. Show all interns");
			System.out.println("6. Show all experiences");
			System.out.println("7. Show all freshers");
			System.out.println("8. Exit");
			System.out.println("****************************");
			System.out.print("Select: ");

			int choice = sc.nextInt();
			sc.nextLine();

			switch (choice) {
			case 1:
				try {
					addEmployee(sc, employees);
				} catch (BirthDayException | PhoneException | EmailException e) {
					e.printStackTrace();
				}
				break;

			case 2:
				editEmployee(sc, employees);
				break;

			case 3:
				deleteEmployee(sc, employees);
				break;

			case 4:
				showAllEmployees(employees);
				break;

			case 5:
				showAllInterns(employees);
				break;

			case 6:
				showAllExperiences(employees);
				break;

			case 7:
				showAllFreshers(employees);
				break;

			case 8:
				System.out.println("Exiting...");
				return;

			default:
				System.out.println("Invalid choice. Please select again.");
				break;
			}
		}
	}

	// thêm mới nhân viên
	private static void addEmployee(Scanner sc, ArrayList<Employee> employees)
			throws BirthDayException, PhoneException, EmailException {
		System.out.println("Enter employee type (0: Experience, 1: Fresher, 2: Intern): ");
		int employeeType = sc.nextInt();
		sc.nextLine();

		Employee newEmployee = null;

		switch (employeeType) {
		case 0:
			newEmployee = addExperience(sc);
			break;

		case 1:
			newEmployee = addFresher(sc);
			break;

		case 2:
			newEmployee = addIntern(sc);
			break;

		default:
			System.out.println("Invalid employee type.");
			return;
		}

		if (newEmployee != null) {
			employees.add(newEmployee);
			System.out.println("Employee added successfully.");
		}
	}

	private static Experience addExperience(Scanner sc) throws BirthDayException, PhoneException, EmailException {

		System.out.print("Enter ID: ");
		int id = sc.nextInt();
		sc.nextLine();

		System.out.print("Enter full name: ");
		String fullName = sc.nextLine();

		System.out.print("Enter birthday (yyyy-mm-dd): ");
		String birthdayStr = sc.nextLine();
		Date birthDay = Utils.parseDate(birthdayStr);

		if (birthDay.after(new Date())) {
			throw new BirthDayException("Invalid birthdate. Birthdate cannot be in the future.");
		}

		System.out.print("Enter phone: ");
		String phone = sc.nextLine();

		if (phone.length() != 10) {
			throw new PhoneException("Invalid phone number. Phone number must be 10 digits.");
		}

		System.out.print("Enter email : ");
		String email = sc.nextLine();

		// regex
		if (!email.contains("@")) {
			throw new EmailException("Invalid email address. Email must contain the '@' symbol.");
		}

		System.out.print("Enter years of experience: ");
		int expInYear = sc.nextInt();
		sc.nextLine();

		System.out.print("Enter professional skill: ");
		String proSkill = sc.nextLine();

		return new Experience(id, fullName, birthDay, phone, email, id, expInYear, proSkill);
	}

	// add fresher
	private static Fresher addFresher(Scanner sc) throws BirthDayException, PhoneException, EmailException {
		System.out.print("Enter ID: ");
		int ID = sc.nextInt();
		sc.nextLine();

		System.out.print("Enter Full Name: ");
		String fullName = sc.nextLine();

		System.out.print("Enter BirthDay (yyyy-mm-dd): ");
		String birthDayStr = sc.nextLine();
		Date birthDay = Utils.parseDate(birthDayStr);

		if (birthDay.after(new Date())) {
			throw new BirthDayException("Invalid birthdate. Birthdate cannot be in the future.");
		}

		System.out.print("Enter Phone: ");
		String phone = sc.nextLine();

		if (phone.length() != 10) {
			throw new PhoneException("Invalid phone number. Phone number must be 10 digits.");
		}

		System.out.print("Enter Email: ");
		String email = sc.nextLine();

		if (!email.contains("@")) {
			throw new EmailException("Invalid email address. Email must contain the '@' symbol.");
		}

		System.out.print("Enter graduation date (yyyy-mm-dd): ");
		String graduationDateStr = sc.nextLine();
		Date graduationDate = Utils.parseDate(graduationDateStr);

		if (graduationDate.after(new Date())) {
			throw new BirthDayException("Invalid graduationDate. GraduationDate cannot be in the future.");
		}

		System.out.print("Enter graduation rank: ");
		String graduationRank = sc.nextLine();

		System.out.print("Enter education: ");
		String education = sc.nextLine();

		return new Fresher(ID, fullName, birthDay, phone, email, ID, graduationDate, graduationRank, education);
	}

	// add Intern
	public static Intern addIntern(Scanner sc) throws BirthDayException, PhoneException, EmailException {
		System.out.println("Enter ID: ");
		int ID = sc.nextInt();
		sc.nextLine();

		System.out.print("Enter Full Name: ");
		String fullName = sc.nextLine();

		System.out.print("Enter BirthDay (yyyy-mm-dd): ");
		String birthDayStr = sc.nextLine();
		Date birthDay = Utils.parseDate(birthDayStr);

		if (birthDay.after(new Date())) {
			throw new BirthDayException("Invalid birthdate. Birthdate cannot be in the future.");
		}

		System.out.print("Enter Phone: ");
		String phone = sc.nextLine();

		if (phone.length() != 10) {
			throw new PhoneException("Invalid phone number. Phone number must be 10 digits.");
		}

		System.out.print("Enter Email: ");
		String email = sc.nextLine();

		if (!email.contains("@")) {
			throw new EmailException("Invalid email address. Email must contain the '@' symbol.");
		}

		System.out.print("Enter majors: ");
		String majors = sc.nextLine();

		System.out.print("Enter semester: ");
		String semester = sc.nextLine();

		System.out.print("Enter university name: ");
		String universityName = sc.nextLine();

		return new Intern(ID, fullName, birthDay, phone, email, ID, majors, semester, universityName);
	}

	// edit employee
	private static void editEmployee(Scanner sc, ArrayList<Employee> employees) {
		System.out.print("Enter employee ID to edit: ");
		int id = sc.nextInt();
		sc.nextLine();

		Employee employeeToEdit = findEmployeeByID(employees, id);

		if (employeeToEdit == null) {
			System.out.println("Employee not found.");
			return;
		}

		System.out.println("Editing employee:");
		employeeToEdit.showInfo();

		System.out.print("Enter new Full Name: ");
		String fullName = sc.nextLine();

		System.out.print("Enter new BirthDay (yyyy-mm-dd): ");
		String birthDayStr = sc.nextLine();
		Date birthDay = Utils.parseDate(birthDayStr);

		if (birthDay.after(new Date())) {
			try {
				throw new BirthDayException("Invalid birthdate. Birthdate cannot be in the future.");
			} catch (BirthDayException e) {
				System.out.println(e.getMessage());
			}
		}

		System.out.print("Enter new Phone: ");
		String phone = sc.nextLine();

		if (phone.length() != 10) {
			try {
				throw new PhoneException("Invalid phone number. Phone number must be 10 digits.");
			} catch (PhoneException e) {
				System.out.println(e.getMessage());
			}
		}

		System.out.print("Enter new Email: ");
		String email = sc.nextLine();

		if (!email.contains("@")) {
			try {
				throw new EmailException("Invalid email address. Email must contain the '@' symbol.");
			} catch (EmailException e) {
				System.out.println(e.getMessage());
			}
		}

		employeeToEdit.setFullName(fullName);
		employeeToEdit.setBirthDay(birthDay);
		employeeToEdit.setPhone(phone);
		employeeToEdit.setEmail(email);

		System.out.println("Employee information updated successfully.");
	}

	// delete employee
	private static void deleteEmployee(Scanner scanner, ArrayList<Employee> employees) {
		System.out.println("Enter employee ID to delete: ");
		int ID = scanner.nextInt();
		scanner.nextLine();

		Employee employeeToDelete = findEmployeeByID(employees, ID);

		if (employeeToDelete == null) {
			System.out.println("Employee not found.");
			return;
		}

		employees.remove(employeeToDelete);
		System.out.println("Employee deleted successfully.");
	}

	private static void showAllEmployees(ArrayList<Employee> employees) {
		for (Employee employee : employees) {
			employee.showInfo();
			System.out.println("----------------------------");
		}
	}

	private static void showAllInterns(ArrayList<Employee> employees) {
		for (Employee employee : employees) {
			if (employee instanceof Intern) {
				employee.showInfo();
				System.out.println("----------------------------");
			}
		}
	}

	private static void showAllExperiences(ArrayList<Employee> employees) {
		for (Employee employee : employees) {
			if (employee instanceof Experience) {
				employee.showInfo();
				System.out.println("----------------------------");
			}
		}
	}

	private static void showAllFreshers(ArrayList<Employee> employees) {
		for (Employee employee : employees) {
			if (employee instanceof Fresher) {
				employee.showInfo();
				System.out.println("----------------------------");
			}
		}
	}

	// find employee by id
	private static Employee findEmployeeByID(ArrayList<Employee> employees, int id) {
		System.out.println("---------------------");
		for (Employee employee : employees) {
			if (employee.getId() == id) {
				return employee;
			}
		}
		return null;
	}

}
