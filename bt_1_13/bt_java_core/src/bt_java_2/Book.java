/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_2;

public class Book extends Document {

	private String authorName;
	private int numberOfPages;

	public Book(String documentCode, String publisherName, int numberOfCopies, String authorName, int numberOfPages) {
		super(documentCode, publisherName, numberOfCopies);
		this.authorName = authorName;
		this.numberOfPages = numberOfPages;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

}
