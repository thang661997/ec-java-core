/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_2;

import java.util.ArrayList;
import java.util.List;

public class DocumentManager {

	private List<Document> documentList;

	public DocumentManager() {
		documentList = new ArrayList<>();
	}

	public void addDocument(Document document) {
		documentList.add(document);
		System.out.println("Document has been added.");
	}

	public void deleteDocument(String documentCode) {
		for (Document document : documentList) {
			if (document.getDocumentCode().equals(documentCode)) {
				documentList.remove(document);
				System.out.println("Document has been deleted.");
				return;
			}
		}
		System.out.println("Document with code " + documentCode + "not found.");
	}

	public void displayDocumentInformation() {
		for (Document document : documentList) {
			if (document instanceof Book) {
				System.out.println("Document Type: Book");
			} else if (document instanceof Magazine) {
				System.out.println("Document Type: Magazine");
			} else if (document instanceof Newspaper) {
				System.out.println("Document Type: Newspaper");
			}

			System.out.println("Document Code: " + document.getDocumentCode());
			System.out.println("Publisher Name: " + document.getPublisherName());
			System.out.println("Number of Copies: " + document.getNumberOfCopies());

			if (document instanceof Book) {
				Book book = (Book) document;
				System.out.println("Author Name: " + book.getAuthorName());
				System.out.println("Number of Pages: " + book.getNumberOfPages());
			} else if (document instanceof Magazine) {
				Magazine magazine = (Magazine) document;
				System.out.println("Issue Number: " + magazine.getIssueNumber());
				System.out.println("Publication Month: " + magazine.getPublicationMonth());
			} else if (document instanceof Newspaper) {
				Newspaper newspaper = (Newspaper) document;
				System.out.println("Publication Date: " + newspaper.getPublicationDate());
			}

			System.out.println("-------------------------");
		}
	}

	public List<Document> searchByType(String type) {
		List<Document> searchResult = new ArrayList<>();
		for (Document document : documentList) {
			if ((type.equalsIgnoreCase("Book") && document instanceof Book)
					|| (type.equalsIgnoreCase("Magazine") && document instanceof Magazine)
					|| (type.equalsIgnoreCase("Newspaper") && document instanceof Newspaper)) {
				searchResult.add(document);
			}
		}
		return searchResult;
	}
}
