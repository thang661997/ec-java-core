/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_2;

import java.util.List;
import java.util.Scanner;

public class Manager {

	public static void main(String[] args) {
		DocumentManager documentManager = new DocumentManager();
		Scanner scanner = new Scanner(System.in);

		while (true) {
			System.out.println("****************************");
			System.out.println("1. Add Document");
			System.out.println("2. Delete Document");
			System.out.println("3. Display Document Information");
			System.out.println("4. Search by Type");
			System.out.println("5. Exit");
			System.out.println("****************************");
			System.out.print("Select: ");

			int choice = scanner.nextInt();
			scanner.nextLine();

			switch (choice) {
			case 1:
				System.out.println("Select Document Type (Book, Magazine, Newspaper): ");
				String documentType = scanner.nextLine();
				System.out.print("Enter Document Code: ");
				String documentCode = scanner.nextLine();
				System.out.print("Enter Publisher Name: ");
				String publisherName = scanner.nextLine();
				System.out.print("Enter Number of Copies: ");
				int numberOfCopies = scanner.nextInt();
				scanner.nextLine();

				if (documentType.equalsIgnoreCase("Book")) {
					System.out.print("Enter Author Name: ");
					String authorName = scanner.nextLine();
					System.out.print("Enter Number of Pages: ");
					int numberOfPages = scanner.nextInt();
					scanner.nextLine();
					documentManager.addDocument(
							new Book(documentCode, publisherName, numberOfCopies, authorName, numberOfPages));
				} else if (documentType.equalsIgnoreCase("Magazine")) {
					System.out.print("Enter Issue Number: ");
					int issueNumber = scanner.nextInt();
					scanner.nextLine();
					System.out.print("Enter Publication Month: ");
					int publicationMonth = scanner.nextInt();
					scanner.nextLine();
					documentManager.addDocument(
							new Magazine(documentCode, publisherName, numberOfCopies, issueNumber, publicationMonth));
				} else if (documentType.equalsIgnoreCase("Newspaper")) {
					System.out.print("Enter Publication Date: ");
					String publicationDate = scanner.nextLine();
					documentManager
							.addDocument(new Newspaper(documentCode, publisherName, numberOfCopies, publicationDate));
				} else {
					System.out.println("Invalid document type.");
				}
				break;

			case 2:
				System.out.print("Enter Document Code to Delete: ");
				String documentCodeToDelete = scanner.nextLine();
				documentManager.deleteDocument(documentCodeToDelete);
				break;

			case 3:
				documentManager.displayDocumentInformation();
				break;
			case 4:
				System.out.print("Enter Document Type to Search (Book, Magazine, Newspaper): ");
				String typeToSearch = scanner.nextLine();
				List<Document> searchResults = documentManager.searchByType(typeToSearch);
				if (searchResults.isEmpty()) {
					System.out.println("No documents of type " + typeToSearch + " found.");
				} else {
					System.out.println("Search Results:");
					for (Document document : searchResults) {
						System.out.println("Document Code: " + document.getDocumentCode());
						System.out.println("Publisher Name: " + document.getPublisherName());
						System.out.println("Number of Copies: " + document.getNumberOfCopies());
						if (document instanceof Book) {
							Book book = (Book) document;
							System.out.println("Author Name: " + book.getAuthorName());
							System.out.println("Number of Pages: " + book.getNumberOfPages());
						} else if (document instanceof Magazine) {
							Magazine magazine = (Magazine) document;
							System.out.println("Issue Number: " + magazine.getIssueNumber());
							System.out.println("Publication Month: " + magazine.getPublicationMonth());
						} else if (document instanceof Newspaper) {
							Newspaper newspaper = (Newspaper) document;
							System.out.println("Publication Date: " + newspaper.getPublicationDate());
						}
						System.out.println("-------------------------");
					}
				}
				break;
			case 5:
				System.out.println("Exiting the program.");
				scanner.close();
				System.exit(0);
			default:
				System.out.println("Invalid choice.");

			}
		}
	}
}
