/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_2;

public class Magazine extends Document {

	private int issueNumber;
	private int publicationMonth;

	public Magazine(String documentCode, String publisherName, int numberOfCopies, int issueNumber,
			int publicationMonth) {
		super(documentCode, publisherName, numberOfCopies);
		this.issueNumber = issueNumber;
		this.publicationMonth = publicationMonth;
	}

	public int getIssueNumber() {
		return issueNumber;
	}

	public void setIssueNumber(int issueNumber) {
		this.issueNumber = issueNumber;
	}

	public int getPublicationMonth() {
		return publicationMonth;
	}

	public void setPublicationMonth(int publicationMonth) {
		this.publicationMonth = publicationMonth;
	}

}
