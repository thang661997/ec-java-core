/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_2;

public class Document {

	private String documentCode;
	private String publisherName;
	private int numberOfCopies;

	public Document(String documentCode, String publisherName, int numberOfCopies) {
		this.documentCode = documentCode;
		this.publisherName = publisherName;
		this.numberOfCopies = numberOfCopies;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public int getNumberOfCopies() {
		return numberOfCopies;
	}

	public void setNumberOfCopies(int numberOfCopies) {
		this.numberOfCopies = numberOfCopies;
	}

}
