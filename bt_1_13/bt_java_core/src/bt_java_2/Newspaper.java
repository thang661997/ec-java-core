/**
 * @author ThangHT8
 * @date 16 Sept 2023
 * @version 1.0
 */

package bt_java_2;

public class Newspaper extends Document {

	private String publicationDate;

	public Newspaper(String documentCode, String publisherName, int numberOfCopies, String publicationDate) {
		super(documentCode, publisherName, numberOfCopies);
		this.publicationDate = publicationDate;
	}

	public String getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}

}
