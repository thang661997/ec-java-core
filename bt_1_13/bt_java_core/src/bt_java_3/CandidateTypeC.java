/**
 * @author ThangHT8
 * @date 22 Sept 2023
 * @version 1.0
 */

package bt_java_3;

public class CandidateTypeC extends Candidate {

	private double literatureScore;
	private double historyScore;
	private double geographyScore;

	public CandidateTypeC(String candidateNumber, String fullName, String address, int priorityLevel,
			double literatureScore, double historyScore, double geographyScore) {
		super(candidateNumber, fullName, address, priorityLevel);
		this.literatureScore = literatureScore;
		this.historyScore = historyScore;
		this.geographyScore = geographyScore;
	}

	public double getLiteratureScore() {
		return literatureScore;
	}

	public void setLiteratureScore(double literatureScore) {
		this.literatureScore = literatureScore;
	}

	public double getHistoryScore() {
		return historyScore;
	}

	public void setHistoryScore(double historyScore) {
		this.historyScore = historyScore;
	}

	public double getGeographyScore() {
		return geographyScore;
	}

	public void setGeographyScore(double geographyScore) {
		this.geographyScore = geographyScore;
	}

}
