/**
 * @author ThangHT8
 * @date 22 Sept 2023
 * @version 1.0
 */

package bt_java_3;

public class CandidateTypeA extends Candidate {

	private double mathScore;
	private double physicsScore;
	private double chemistryScore;

	public CandidateTypeA(String candidateNumber, String fullName, String address, int priorityLevel, double mathScore,
			double physicsScore, double chemistryScore) {
		super(candidateNumber, fullName, address, priorityLevel);
		this.mathScore = mathScore;
		this.physicsScore = physicsScore;
		this.chemistryScore = chemistryScore;
	}

	public double getMathScore() {
		return mathScore;
	}

	public void setMathScore(double mathScore) {
		this.mathScore = mathScore;
	}

	public double getPhysicsScore() {
		return physicsScore;
	}

	public void setPhysicsScore(double physicsScore) {
		this.physicsScore = physicsScore;
	}

	public double getChemistryScore() {
		return chemistryScore;
	}

	public void setChemistryScore(double chemistryScore) {
		this.chemistryScore = chemistryScore;
	}

}
