/**
 * @author ThangHT8
 * @date 22 Sept 2023
 * @version 1.0
 */

package bt_java_3;

import java.util.ArrayList;
import java.util.List;

public class Admission {

	private List<Candidate> candidateList;

	public Admission() {
		candidateList = new ArrayList<>();
	}

	public void addCandidate(Candidate candidate) {
		candidateList.add(candidate);
	}

	public void displayCandidateInfo() {
		for (Candidate candidate : candidateList) {
			if (candidate instanceof CandidateTypeA) {

				System.out.println("Candidate Type A:");
				System.out.println("Candidate Number: " + candidate.getCandidateNumber());
				System.out.println("Full Name: " + candidate.getFullName());

				// Display scores for Type A candidates
				CandidateTypeA candidateTypeA = (CandidateTypeA) candidate;
				System.out.println("Math Score: " + candidateTypeA.getMathScore());
				System.out.println("Physics Score: " + candidateTypeA.getPhysicsScore());
				System.out.println("Chemistry Score: " + candidateTypeA.getChemistryScore());

			} else if (candidate instanceof CandidateTypeB) {

				System.out.println("Candidate Type B:");
				System.out.println("Candidate Number: " + candidate.getCandidateNumber());
				System.out.println("Full Name: " + candidate.getFullName());

				// Display scores for Type B candidates
				CandidateTypeB candidateTypeB = (CandidateTypeB) candidate;
				System.out.println("Math Score: " + candidateTypeB.getMathScore());
				System.out.println("Physics Score: " + candidateTypeB.getBiologyScore());
				System.out.println("Chemistry Score: " + candidateTypeB.getChemistryScore());

			} else if (candidate instanceof CandidateTypeC) {

				System.out.println("Candidate Type C:");
				System.out.println("Candidate Number: " + candidate.getCandidateNumber());
				System.out.println("Full Name: " + candidate.getFullName());

				// Display scores for Type C candidates
				CandidateTypeC candidateTypeC = (CandidateTypeC) candidate;
				System.out.println("Math Score: " + candidateTypeC.getLiteratureScore());
				System.out.println("Physics Score: " + candidateTypeC.getHistoryScore());
				System.out.println("Chemistry Score: " + candidateTypeC.getGeographyScore());

			}
			System.out.println("Address: " + candidate.getAddress());
			System.out.println("Priority Level: " + candidate.getPriorityLevel());
		}
	}

	public Candidate searchByCandidateNumber(String candidateNumber) {
		for (Candidate candidate : candidateList) {
			if (candidate.getCandidateNumber().equals(candidateNumber)) {
				return candidate;
			}
		}
		return null;
	}
}
