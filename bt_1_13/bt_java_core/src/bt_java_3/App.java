/**
 * @author ThangHT8
 * @date 22 Sept 2023
 * @version 1.0
 */

package bt_java_3;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Admission admission = new Admission();

		while (true) {
			System.out.println("******************************");
			System.out.println("Welcome to the university admission management program");
			System.out.println("Please choose an option:");
			System.out.println("1. Add a new candidate");
			System.out.println("2. Display candidate information and exam type");
			System.out.println("3. Search by candidate number");
			System.out.println("4. Exit");
			System.out.println("******************************");

			int choice;
			System.out.print("Enter your choice: ");
			try {
				choice = sc.nextInt();
				sc.nextLine();
			} catch (NumberFormatException e) {
				System.out.println("Invalid choice. Please try again.");
				continue;
			}
			
			System.out.println("******************************");

			switch (choice) {
			case 1:
				addNewCandidate(sc, admission);
				break;

			case 2:
				admission.displayCandidateInfo();
				break;
				
			case 3:
				searchByCandidateNumber(sc, admission);
				break;
				
			case 4:
				System.out.println("Exiting the program.");
				sc.close();
				System.exit(0);
				
			default:
				System.out.println("Invalid choice. Please try again.");
			}
		}
	}

	private static void addNewCandidate(Scanner sc, Admission admission) {
		System.out.println("Enter candidate information: ");
		System.out.print("Candidate Number: ");
		String candidateNumber = sc.nextLine();
		System.out.print("Full Name: ");
		String fullName = sc.nextLine();
		System.out.print("Address: ");
		String address = sc.nextLine();
		System.out.print("Priority Level: ");
		int priorityLevel = sc.nextInt();
		sc.nextLine();

		System.out.println("Choose exam type (A/B/C): ");
		String examType = sc.nextLine();

		switch (examType) {
		case "A":
			double mathScore, physicsScore, chemistryScore;
			System.out.print("Math Score: ");
			mathScore = sc.nextDouble();
			System.out.print("Physics Score: ");
			physicsScore = sc.nextDouble();
			System.out.print("Chemistry Score: ");
			chemistryScore = sc.nextDouble();
			sc.nextLine();

			CandidateTypeA candidateTypeA = new CandidateTypeA(candidateNumber, fullName, address, priorityLevel,
					mathScore, physicsScore, chemistryScore);
			admission.addCandidate(candidateTypeA);
			break;

		case "B":
			double mathScoreB, chemistryScoreB, biologyScore;
			System.out.print("Math Score: ");
			mathScoreB = sc.nextDouble();
			System.out.print("Chemistry Score: ");
			chemistryScoreB = sc.nextDouble();
			System.out.print("Biology Score: ");
			biologyScore = sc.nextDouble();
			sc.nextLine();

			CandidateTypeB candidateTypeB = new CandidateTypeB(candidateNumber, fullName, address, priorityLevel,
					mathScoreB, chemistryScoreB, biologyScore);
			admission.addCandidate(candidateTypeB);
			break;

		case "C":
			double literatureScore, historyScore, geographyScore;
			System.out.print("Literature Score: ");
			literatureScore = sc.nextDouble();
			System.out.print("History Score: ");
			historyScore = sc.nextDouble();
			System.out.print("Geography Score: ");
			geographyScore = sc.nextDouble();
			sc.nextLine();

			CandidateTypeC candidateTypeC = new CandidateTypeC(candidateNumber, fullName, address, priorityLevel,
					literatureScore, historyScore, geographyScore);
			admission.addCandidate(candidateTypeC);
			break;

		default:
			System.out.println("Invalid exam type.");
		}
	}

	private static void searchByCandidateNumber(Scanner sc, Admission admission) {
		System.out.print("Enter candidate number to search: ");
		String candidateNumber = sc.nextLine();
		Candidate candidate = admission.searchByCandidateNumber(candidateNumber);
		if (candidate != null) {
			System.out.println("Candidate information with candidate number " + candidateNumber + ":");
			System.out.println("Full Name: " + candidate.getFullName());
			System.out.println("Exam Type: " + getExamType(candidate));
		} else {
			System.out.println("Candidate with candidate number " + candidateNumber + " not found.");
		}
	}

	private static String getExamType(Candidate candidate) {
		if (candidate instanceof CandidateTypeA) {
			return "A";
		} else if (candidate instanceof CandidateTypeB) {
			return "B";
		} else if (candidate instanceof CandidateTypeC) {
			return "C";
		}
		return "Unknown";
	}
}
