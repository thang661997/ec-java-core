/**
 * @author ThangHT8
 * @date 22 Sept 2023
 * @version 1.0
 */

package bt_java_3;

public class CandidateTypeB extends Candidate {

	private double mathScore;
	private double chemistryScore;
	private double biologyScore;

	public CandidateTypeB(String candidateNumber, String fullName, String address, int priorityLevel, double mathScore,
			double chemistryScore, double biologyScore) {
		super(candidateNumber, fullName, address, priorityLevel);
		this.mathScore = mathScore;
		this.chemistryScore = chemistryScore;
		this.biologyScore = biologyScore;
	}

	public double getMathScore() {
		return mathScore;
	}

	public void setMathScore(double mathScore) {
		this.mathScore = mathScore;
	}

	public double getChemistryScore() {
		return chemistryScore;
	}

	public void setChemistryScore(double chemistryScore) {
		this.chemistryScore = chemistryScore;
	}

	public double getBiologyScore() {
		return biologyScore;
	}

	public void setBiologyScore(double biologyScore) {
		this.biologyScore = biologyScore;
	}

}
