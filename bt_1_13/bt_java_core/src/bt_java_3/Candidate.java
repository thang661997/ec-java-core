/**
 * @author ThangHT8
 * @date 22 Sept 2023
 * @version 1.0
 */

package bt_java_3;

public abstract class Candidate {

	private String candidateNumber;
	private String fullName;
	private String address;
	private int priorityLevel;

	public Candidate(String candidateNumber, String fullName, String address, int priorityLevel) {
		super();
		this.candidateNumber = candidateNumber;
		this.fullName = fullName;
		this.address = address;
		this.priorityLevel = priorityLevel;
	}

	public String getCandidateNumber() {
		return candidateNumber;
	}

	public void setCandidateNumber(String candidateNumber) {
		this.candidateNumber = candidateNumber;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(int priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

}
