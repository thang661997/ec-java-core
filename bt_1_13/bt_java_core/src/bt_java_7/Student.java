/**
 * @author ThangHT8
 * @date 14 Sept 2023
 * @version 1.0
 */

package bt_java_7;

import java.util.List;
import java.util.Scanner;

public class Student extends Person {

	private String studentId;

	public Student() {
	}

	public Student(String fullName, int age, String hometown, String studentId) {
		super(fullName, age, hometown);
		this.studentId = studentId;
	}
	
	public static Student createStudent(Scanner scanner) {
        System.out.print("Enter student's full name: ");
        String fullName = scanner.nextLine();

        System.out.print("Enter age: ");
        int age = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Enter hometown: ");
        String hometown = scanner.nextLine();

        System.out.print("Enter your student code: ");
        String studentId = scanner.nextLine();

        return new Student(fullName, age, hometown, studentId);
    }
	
	public void removeStudentById(List<Student> students, String studentId) {
		Student teacherToRemove = null;
		for(Student student : students) {
			if(student.getStudentId().equals(studentId)) {
				teacherToRemove = student;
				break;
			}
		}
		
		if(teacherToRemove != null) {
			students.remove(teacherToRemove);
			System.out.println("The student has been cleared.");
		} else {
			System.out.println("No student found with student code: " + studentId);
		}
	}

	public void displayStudentInformation() {
	    System.out.println("Student information: ");
	    System.out.println("Full name: " + getFullName());
	    System.out.println("Age: " + getAge());
	    System.out.println("Hometown: " + getHometown());
	    System.out.println("Student code: " + getStudentId());
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

}
