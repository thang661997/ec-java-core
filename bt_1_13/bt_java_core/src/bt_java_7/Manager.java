/**
 * @author ThangHT8
 * @date 14 Sept 2023
 * @version 1.0
 */

package bt_java_7;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Manager {

	public static void main(String[] args) {
		List<Teacher> teachers = new ArrayList<>();
		List<Student> students = new ArrayList<>();
		List<Classroom> classrooms = new ArrayList<>();

		classrooms.add(new Classroom("Math Class"));
		classrooms.add(new Classroom("English Class"));

		Scanner sc = new Scanner(System.in);

		Teacher teacher;
		Student student;
		boolean found;

		while (true) {
			System.out.println("************************");
			System.out.println("1. Add teacher");
            System.out.println("2. Delete teachers by teacher code");
            System.out.println("3. Display teacher information by teacher code");
            System.out.println("4. Add student");
            System.out.println("5. Delete students by student number");
            System.out.println("6. Display student information by student code");
            System.out.println("7. Display classrooms");
            System.out.println("8. Add teacher or student to classroom");
            System.out.println("9. Exit");
            System.out.println("******************************");
            System.out.print("Select: ");

			int choice = sc.nextInt();
			sc.nextLine();

			System.out.println("******************************");

			switch (choice) {
			case 1:
				teacher = Teacher.createTeacher(sc);
				teachers.add(teacher);
				System.out.println("Teacher has been added.");
				break;

			case 2:
				System.out.print("Enter teacher code to delete: ");
				String teacherIdToDelete = sc.nextLine();
				teacher = new Teacher();
				teacher.removeTeacherById(teachers, teacherIdToDelete);
				break;

			case 3:
				System.out.print("Enter teacher code to display information: ");
				String teacherIdToDisplay = sc.nextLine();
				found = false;
				for (Teacher t : teachers) {
					if (t.getTeacherId().equals(teacherIdToDisplay)) {
						t.displayTeacherInformation();
						found = true;
						break;
					}
				}
				if (!found) {
					System.out.println("No teacher found with teacher code: " + teacherIdToDisplay);
				}
				break;

			case 4:
				student = Student.createStudent(sc);
				students.add(student);
				System.out.println("Student has been added.");
				break;

			case 5:
				System.out.print("Enter student code to delete: ");
				String studentIdToDelete = sc.nextLine();
				student = new Student();
				student.removeStudentById(students, studentIdToDelete);
				break;

			case 6:
				System.out.print("Enter student code to display information: ");
				String studentIdToDisplay = sc.nextLine();
				found = false;
				for (Student s : students) {
					if (s.getStudentId().equals(studentIdToDisplay)) {
						s.displayStudentInformation();
						found = true;
						break;
					}
				}
				if (!found) {
					System.out.println("No student found with student code: " + studentIdToDisplay);
				}
				break;

			case 7:
				System.out.println("Classrooms:");
				for (int i = 0; i < classrooms.size(); i++) {
					System.out.println((i + 1) + ". " + classrooms.get(i).getClassName());
				}
				break;

			case 8:
				Classroom.addEntityToClassroom(classrooms, teachers, students, sc);
				break;

			case 9:
				System.out.println("Application ended.");
				sc.close();
				System.exit(0);

			default:
				System.out.println("Invalid selection.");
			}
		}
	}
}
