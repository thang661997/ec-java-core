/**
 * @author ThangHT8
 * @date 14 Sept 2023
 * @version 1.0
 */

package bt_java_7;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Classroom {

	private String className;
	private List<Teacher> teachers;
	private List<Student> students;
	
	public Classroom() {
	}

	public Classroom(String className) {
		this.className = className;
		this.teachers = new ArrayList<>();
		this.students = new ArrayList<>();
	}

	public void addTeacher(Teacher teacher) {
		teachers.add(teacher);
	}

	public void addStudent(Student student) {
		students.add(student);
	}
	
	public static void addEntityToClassroom(List<Classroom> classrooms, List<Teacher> teachers, List<Student> students, Scanner sc) {
	    System.out.print("Select a classroom (1-" + classrooms.size() + "): ");
	    int classroomIndex = sc.nextInt();
	    sc.nextLine();

	    if (classroomIndex >= 1 && classroomIndex <= classrooms.size()) {
	        Classroom selectedClassroom = classrooms.get(classroomIndex - 1);
	        System.out.print("Add a Teacher (1) or a Student (2): ");
	        int entityType = sc.nextInt();
	        sc.nextLine();

	        if (entityType == 1) {
	            if (!teachers.isEmpty()) {
	                System.out.println("Teachers:");
	                for (int i = 0; i < teachers.size(); i++) {
	                    System.out.println((i + 1) + ". " + teachers.get(i).getFullName());
	                }
	                System.out.print("Select a teacher (1-" + teachers.size() + "): ");
	                int teacherIndex = sc.nextInt();
	                sc.nextLine();

	                if (teacherIndex >= 1 && teacherIndex <= teachers.size()) {
	                    Teacher selectedTeacher = teachers.get(teacherIndex - 1);
	                    selectedClassroom.addTeacher(selectedTeacher);
	                    System.out.println("Teacher added to " + selectedClassroom.getClassName());
	                } else {
	                    System.out.println("Invalid teacher selection.");
	                }
	            } else {
	                System.out.println("No teachers available.");
	            }
	        } else if (entityType == 2) {
	            if (!students.isEmpty()) {
	                System.out.println("Students:");
	                for (int i = 0; i < students.size(); i++) {
	                    System.out.println((i + 1) + ". " + students.get(i).getFullName());
	                }
	                System.out.print("Select a student (1-" + students.size() + "): ");
	                int studentIndex = sc.nextInt();
	                sc.nextLine();

	                if (studentIndex >= 1 && studentIndex <= students.size()) {
	                    Student selectedStudent = students.get(studentIndex - 1);
	                    selectedClassroom.addStudent(selectedStudent);
	                    System.out.println("Student added to " + selectedClassroom.getClassName());
	                } else {
	                    System.out.println("Invalid student selection.");
	                }
	            } else {
	                System.out.println("No students available.");
	            }
	        } else {
	            System.out.println("Invalid entity type.");
	        }
	    } else {
	        System.out.println("Invalid classroom selection.");
	    }
	}


	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public List<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

}
