/**
 * @author ThangHT8
 * @date 14 Sept 2023
 * @version 1.0
 */

package bt_java_7;

import java.util.List;
import java.util.Scanner;

public class Teacher extends Person {

	private String teacherId;
	private int baseSalary;
	private int bonusSalary;
	private int penaltySalary;

	public Teacher() {
	}

	public Teacher(String fullName, int age, String hometown, String teacherId, int baseSalary, int bonusSalary,
			int penaltySalary) {
		super(fullName, age, hometown);
		this.teacherId = teacherId;
		this.baseSalary = baseSalary;
		this.bonusSalary = bonusSalary;
		this.penaltySalary = penaltySalary;
	}
	
	public static Teacher createTeacher(Scanner scanner) {
        System.out.print("Enter teacher's full name: ");
        String fullName = scanner.nextLine();

        System.out.print("Enter age: ");
        int age = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Enter hometown: ");
        String hometown = scanner.nextLine();

        System.out.print("Enter your teacher code: ");
        String teacherId = scanner.nextLine();

        System.out.print("Enter base salary: ");
        int baseSalary = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Enter bonusSalary: ");
        int bonusSalary = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Enter penaltySalary: ");
        int penaltySalary = scanner.nextInt();
        scanner.nextLine();

        return new Teacher(fullName, age, hometown, teacherId, baseSalary, bonusSalary, penaltySalary);
    }
	
	public void removeTeacherById(List<Teacher> teachers, String teacherId) {
		Teacher teacherToRemove = null;
		for(Teacher teacher : teachers) {
			if(teacher.getTeacherId().equals(teacherId)) {
				teacherToRemove = teacher;
				break;
			}
		}
		
		if(teacherToRemove != null) {
			teachers.remove(teacherToRemove);
			System.out.println("The teacher has been cleared.");
		} else {
			System.out.println("No teacher found with teacher code: " + teacherId);
		}
	}

	public void displayTeacherInformation() {
	    System.out.println("Teacher information: ");
	    System.out.println("Full name: " + getFullName());
	    System.out.println("Age: " + getAge());
	    System.out.println("Hometown: " + getHometown());
	    System.out.println("Teacher code: " + getTeacherId());
	    System.out.println("Base Salary: " + getBaseSalary());
	    System.out.println("Bonus Salary: " + getBonusSalary());
	    System.out.println("Penalty Salary: " + getPenaltySalary());
	    System.out.println("Calculate Real Salary: " + calculateRealSalary());
	}

	
	public int calculateRealSalary() {
		return baseSalary + bonusSalary - penaltySalary;
	}
	
	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public int getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(int baseSalary) {
		this.baseSalary = baseSalary;
	}

	public int getBonusSalary() {
		return bonusSalary;
	}

	public void setBonusSalary(int bonusSalary) {
		this.bonusSalary = bonusSalary;
	}

	public int getPenaltySalary() {
		return penaltySalary;
	}

	public void setPenaltySalary(int penaltySalary) {
		this.penaltySalary = penaltySalary;
	}

}
