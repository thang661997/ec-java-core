/**
 * @author ThangHT8
 * @date 14 Sept 2023
 * @version 1.0
 */

package bt_java_7;

public class Person {

	private String fullName;
	private int age;
	private String hometown;
	
	public Person() {
	}

	public Person(String fullName, int age, String hometown) {
		this.fullName = fullName;
		this.age = age;
		this.hometown = hometown;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getHometown() {
		return hometown;
	}

	public void setHometown(String hometown) {
		this.hometown = hometown;
	}

}
